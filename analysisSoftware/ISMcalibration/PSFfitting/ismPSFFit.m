function [over, int, im, xx, yy] = ismPSFFit(img)

rhofield = [0 2];
zfield = 0;
NA = 1.49;
fd = 1800;
n0 = 1.52;
n = n0;
n1 = n0;
d0 = [];
d = max(zfield);
d1 = [];
lamex = 0.64;
overv = [100:100:10000];
focpos = 0;
atf = [];
resolution = [];
ring = [];
maxm = [];
pix = 0.05; % effective pixel size

nn = floor(max(rhofield)/pix/sqrt(2));
f = figure;
ax = axes(f);
imagesc(ax, img);
colormap parula
axis square
rec = drawrectangle(ax);
img = imcrop(img,rec.Position);
imagesc(ax,img)
[b0,a0] = ginput(1);
a = round(a0);
b = round(b0);
if nn>=a
    nn = a-1;
end
if nn>=b
    nn = b-1;
end
if nn>=size(img,1)-a
    nn = size(img,1)-a-1;
end
if nn>=size(img,2)-b
    nn = size(img,2)-b-1;
end
im = img(a-nn:a+nn,b-nn:b+nn);

[xx, yy] = meshgrid(pix*[-nn:nn],pix*[-nn:nn]);
pp = atan2(yy-pix*(b0-b-0.5),xx-pix*(a0-a-0.5));
rr = sqrt((xx-pix*(a0-a-0.5)).^2+(yy-pix*(b0-b-0.5)).^2);

err = zeros(1,numel(overv));
for jover=1:numel(overv)
    over = overv(jover);
    exc = GaussExc(rhofield, zfield, NA, fd, n0, n, n1, d0, d, d1, lamex, ...
        over, focpos, atf, resolution, ring, maxm);
    exc1 = RotateEMField(exc, pi/2);
    exc.fxc = exc.fxc + 1i*exc1.fxc;
    exc.fxs = exc.fxs + 1i*exc1.fxs;
    exc.fyc = exc.fyc + 1i*exc1.fyc;
    exc.fys = exc.fys + 1i*exc1.fys;
    exc.fzc = exc.fzc + 1i*exc1.fzc;
    exc.fzs = exc.fzs + 1i*exc1.fzs;
    
    int = interp1(exc.rho,abs(exc.fxc(:,1,1)).^2+abs(exc.fyc(:,1,1)).^2+abs(exc.fzc(:,1,1)).^2,rr,'pchip',0);
    for j=1:size(exc.fxs,3)
        fx = interp1(exc.rho,exc.fxc(:,1,j+1),rr,'pchip',0).*cos(j*pp) + interp1(exc.rho,exc.fxs(:,1,j),rr,'pchip',0).*sin(j*pp);
        fy = interp1(exc.rho,exc.fyc(:,1,j+1),rr,'pchip',0).*cos(j*pp) + interp1(exc.rho,exc.fys(:,1,j),rr,'pchip',0).*sin(j*pp);
        fz = interp1(exc.rho,exc.fzc(:,1,j+1),rr,'pchip',0).*cos(j*pp) + interp1(exc.rho,exc.fzs(:,1,j),rr,'pchip',0).*sin(j*pp);
        int = int + abs(fx).^2 + abs(fy).^2 + abs(fz).^2;
    end
    c = [ones(numel(im),1) int(:)]\im(:);
    err(jover) = sum(sum((im - c(1) - c(2)*int).^2));
    subplot(121);
    pcolor(xx,yy,im); axis image; colormap hot
    subplot(122);
    pcolor(xx,yy,int); axis image; colormap hot
    drawnow
end
[~,ind] = min(err);
over = overv(ind);

exc = GaussExc(rhofield, zfield, NA, fd, n0, n, n1, d0, d, d1, lamex, ...
    over, focpos, atf, resolution, ring, maxm);
exc1 = RotateEMField(exc, pi/2);
exc.fxc = exc.fxc + 1i*exc1.fxc;
exc.fxs = exc.fxs + 1i*exc1.fxs;
exc.fyc = exc.fyc + 1i*exc1.fyc;
exc.fys = exc.fys + 1i*exc1.fys;
exc.fzc = exc.fzc + 1i*exc1.fzc;
exc.fzs = exc.fzs + 1i*exc1.fzs;

% nn = floor(max(rhofield)/pix/sqrt(2));
% [xx, yy] = meshgrid(pix*[-nn:nn],pix*[-nn:nn]);
pp = atan2(yy,xx);
rr = sqrt(xx.^2+yy.^2);
int = interp1(exc.rho,abs(exc.fxc(:,1,1)).^2+abs(exc.fyc(:,1,1)).^2+abs(exc.fzc(:,1,1)).^2,rr,'pchip',0);
for j=1:size(exc.fxs,3)
    fx = interp1(exc.rho,exc.fxc(:,1,j+1),rr,'pchip',0).*cos(j*pp) + interp1(exc.rho,exc.fxs(:,1,j),rr,'pchip',0).*sin(j*pp);
    fy = interp1(exc.rho,exc.fyc(:,1,j+1),rr,'pchip',0).*cos(j*pp) + interp1(exc.rho,exc.fys(:,1,j),rr,'pchip',0).*sin(j*pp);
    fz = interp1(exc.rho,exc.fzc(:,1,j+1),rr,'pchip',0).*cos(j*pp) + interp1(exc.rho,exc.fzs(:,1,j),rr,'pchip',0).*sin(j*pp);
    int = int + abs(fx).^2 + abs(fy).^2 + abs(fz).^2;
end
pcolor(xx,yy,int); axis image; colormap hot

