%claculate ISM shift Vercores and PSF

fname = 'rawData\Figure 1\STORM_test_gatta_quant_beadsR_50nm_27p9_pix_3.ptu';
dcname = 'rawData\Figure 1\gattaQ_beads_atto647n_1p9mW_OD2_50nm-pix_dark5.ptu';

tmp_name        = strsplit(fname, '\');
folder_name     = tmp_name{end-1};

%read ISM data from .ptu file
[~,~,im_posy,im_posx,~,head] = read_ISM(fname);

ind             = im_posy<=head.ImgHdr_PixY;
im_posx         = im_posx(ind);
im_posy         = im_posy(ind);
im_posx         = im_posx * head.ImgHdr_PixX;

%number of pixels in recorded image
s_pixl_x    = head.ImgHdr_PixX;
s_pixl_y    = head.ImgHdr_PixY;

%Dark count in count per second
[dc, ~] = get_DC(dcname,0);

%time per pixel
if strcmp(head.CreatorSW_Name, 'SymPhoTime 64')
    %timer per pixel in total in seconds
    pix_time = head.ImgHdr_MaxFrames * head.ImgHdr_TimePerPixel/1e3;
else
    pix_time = head.ImgHdr_TimePerPixel/1e3;
end

calibration = struct();

calibration.dc = dc;
%% calculate confocal psf

[sum_img, ~, ~] = img_ps(im_posx, im_posy, s_pixl_x, s_pixl_y,1);
sum_img_dc = max(sum_img - sum(dc) * pix_time, 0);

[over, int, im, xx, yy] = ismPSFFit(sum_img);

calibration.psf     = int;
calibration.over    = over;
%% generate shift Vectors

[shiftVector, save_name] = getShiftVectors(fname, dcname);

calibration.shiftVector = shiftVector;
calibration.svName      = save_name;
calibration.fileName    = fname;
calibration.dc          = dc;
calibration.pixSize     = head.ImgHdr_PixResol;

saveTo = append('ismCallibration', folder_name, '.mat');

save(saveTo,"-struct", "calibration",'-v7.3')