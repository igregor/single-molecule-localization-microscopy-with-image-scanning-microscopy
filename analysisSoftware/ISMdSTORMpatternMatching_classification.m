%% Load the analysed data from the Tubulin-Alexa647 and Actin-Phaloidin-JF655 sample

clear
close all
clc
% Files for reference patterns
tntres_ref  = [...
    load('pattern1_TNT.mat'),...
    load('pattern2_TNT.mat')
    ];

% Files to classify
testfiles = 'testfile_TNT.mat';

% Ground truth (1 == Alexa647, 2 == JF646)
testind = 1*contains(testfiles,'647') + 2*contains(testfiles,'646');

%% Define filter

% Define filter to remove bad localisations (background, multiple emitters),
% same syntax as in TrackNTrace
filter_str = 'sigma<5&nphoton>200';

% Generates filter function
getFilterFun = @(paramsNames)str2func(['@(posData)true(size(posData,1),1)&' regexprep(vectorize(['( ' filter_str ' )']),...
    strcat('(?<!\w)',matlab.lang.makeValidName(paramsNames),'(?!\w)'),... % Replace spaces with underscore and makes sure the match is not within a name.
    cellfun(@(n)sprintf('posData(:,%i)',n),num2cell(1:numel(paramsNames)),'UniformOutput',false),...
    'ignorecase')]);


%% Generate reference decays
rnum = numel(tntres_ref); % number of references

%indeces of tscpc tail
TCSPCtailIndex = tntres_ref(1).postprocOptions.TCSPC_tail(1:end-10);

% Variabel for the reference patterns
tcspc_ref = zeros(size(TCSPCtailIndex,2),rnum); % this only works well if all TCSPC curves have the same length (+/-1) and and peak position

for ridx = 1:rnum  
    % For each reference: find all molecules that pass the filter and sum their
    % TCSPC histograms
    assert(isfield(tntres_ref(ridx).postprocOptions,'TCSPC')); % Make sure TCSPC data is exported
    
    % find index of Track-ID (should usully be 1)
    data_track_ind = find(contains(tntres_ref(ridx).postprocOptions.outParamDescription,'Track-ID','IgnoreCase',true),1);
    
    % get Track-IDs for molecules passing the filter
    filter_ind = feval(getFilterFun(tntres_ref(ridx).postprocOptions.outParamDescription),tntres_ref(ridx).postprocData);
    filter_TrackIDs = unique(tntres_ref(ridx).postprocData(filter_ind,data_track_ind));
    % save summed TCSPC histograms of selected molecules
    tcspc_ref(:,ridx) = sum(tntres_ref(ridx).postprocOptions.TCSPC(filter_TrackIDs,TCSPCtailIndex),1);
end
    
%% Prepare patterns liklihood measure Q for each TCSPC
% Avoid zeros in (too) sparse reference data
tcspc_ref(tcspc_ref==0|isnan(tcspc_ref)) = 0.01;
% calculate log of normalised reference pattern
% (Enderlein&Sauer 2001, eq 3)
ln_p_ref = log(tcspc_ref./sum(tcspc_ref,1));


%% Classify test data
tntres = load(testfiles);
data = struct();

data_track_ind = find(contains(tntres.postprocOptions.outParamDescription,'Track-ID','IgnoreCase',true),1);
assert(~isempty(data_track_ind));
data_tau_ind = find(contains(tntres.postprocOptions.outParamDescription,'lt-tau','IgnoreCase',true),1);
assert(~isempty(data_tau_ind));

% apply filter
filter_ind = find(feval(getFilterFun(tntres.postprocOptions.outParamDescription),tntres.postprocData));

% save index and lifetime of selected molecules
[~,idx] = unique(tntres.postprocData(filter_ind,data_track_ind));

data.Track_ID = tntres.postprocData(filter_ind(idx),data_track_ind);
data.lt_tau = tntres.postprocData(filter_ind(idx),data_tau_ind);

assert(isfield(tntres.postprocOptions,'TCSPC')); % Make sure TCSPC data is exported
% Calculate the likelihood matrix lambda
% track_id is index for TCSPC
Lambda = - tntres.postprocOptions.TCSPC(data.Track_ID,TCSPCtailIndex) * ln_p_ref;
% Find the position of the minima
[~,PM_ind] = min(Lambda,[],2);
fprintf('Pattern Class 1: micotubles with %d localisations\n', sum(PM_ind==1))
fprintf('Pattern Class 2: actin ns with %d localisations\n', sum(PM_ind==2))
% Calculate the posterior proberbility for the nth species
% f_n = exp(-Lambda)./(sum(exp(-Lambda),2)); % P(S1)/(P(S1)+P(S2))
f_n = exp(-Lambda-mean(-Lambda,2))./(sum(exp(-Lambda-mean(-Lambda,2)),2)); % Avoids nans due to overflowing floats

data.PM_ind = PM_ind;
for qidx = 1:size(f_n,2)
    data.(sprintf('f%i',qidx)) = f_n(:,qidx);
end

% save the ground truth
data.true_ind = ones(size(PM_ind)) * testind;
%
tdata = struct2table(data);

if ~contains(tntres.postprocOptions.outParamDescription, 'lt-class')
    tntres.postprocData = [tntres.postprocData zeros(size(tntres.postprocData,1),1)];
    tntres.postprocOptions.outParamDescription{end+1} = 'lt-class';
end
%Iindex of patternmatshing id in data file
data_PM_ind = find(contains(tntres.postprocOptions.outParamDescription,'lt-class','IgnoreCase',true),1);

if ~contains(tntres.postprocOptions.outParamDescription, 'lt-f1')
    tntres.postprocData = [tntres.postprocData zeros(size(tntres.postprocData,1),1)];
    tntres.postprocOptions.outParamDescription{end+1} = 'lt-f1';
end
%Iindex of class 1 prob. in data file
data_f1_ind = find(contains(tntres.postprocOptions.outParamDescription,'lt-f1','IgnoreCase',true),1);

if ~contains(tntres.postprocOptions.outParamDescription, 'lt-f2')
    tntres.postprocData = [tntres.postprocData zeros(size(tntres.postprocData,1),1)];
    tntres.postprocOptions.outParamDescription{end+1} = 'lt-f2';
end
%Iindex of class 2 prob. in data file
data_f2_ind = find(contains(tntres.postprocOptions.outParamDescription,'lt-f2','IgnoreCase',true),1);


%set lt-class for all localisations. use same lt-class for same track id
for i=1:numel(data.Track_ID)
    %find all localistions with same track id
    ind = tntres.postprocData(:,data_track_ind) == data.Track_ID(i);
    
    %save PM id and both probabilites for all tracks with same id
    tntres.postprocData(ind,data_PM_ind) = data.PM_ind(i);
    tntres.postprocData(ind,data_f1_ind) = data.f1(i);
    tntres.postprocData(ind,data_f2_ind) = data.f2(i);
end

saveFile = strsplit(testfiles, '.');
saveTo = append(saveFile{1}, '_pm-class','.', saveFile{2});

TNTdata = tntres;
save(saveTo,"-struct", "TNTdata",'-v7.3')

% tdata = vertcat(tdata);

