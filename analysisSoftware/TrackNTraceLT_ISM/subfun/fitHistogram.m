function varargout = fitHistogram(nModel,binedges,binvalues,initValues)
%FITHISTOGRAM Summary of this function goes here
%   Detailed explanation goes here
%
% [{'list of available models'}, models] = fitHistogram()

%% Define models
inlcudeBGcomponent = true;
clear fitmodels;
fitmodels = struct(...
    'Name','No fit',...
    'FitFunction',@noFit,...
    'FitArguments',{{}},...
    'FitParameter',{{'mean','median','std'}},...
    'FitInitParameter',{{}}...
);

fitmodels(end+1) = struct(...
    'Name','1x Gaussian',...
    'FitFunction',@fitNGaussian,...
    'FitArguments',{{1,~inlcudeBGcomponent}},...
    'FitParameter',{{'amp','mu','sigma'}},...
    'FitInitParameter',{{'mu','sigma'}}...
);

fitmodels(end+1) = struct(...
    'Name','1x Gaussian + BG',...
    'FitFunction',@fitNGaussian,...
    'FitArguments',{{1,inlcudeBGcomponent}},...
    'FitParameter',{{'amp','mu','sigma','bg'}},...
    'FitInitParameter',{{'mu','sigma'}}...
);

fitmodels(end+1) = struct(...
    'Name','2x Gaussian',...
    'FitFunction',@fitNGaussian,...
    'FitArguments',{{2,~inlcudeBGcomponent}},...
    'FitParameter',{{'amp1','mu1','sigma1','amp2','mu2','sigma2'}},...
    'FitInitParameter',{{'mu1','sigma1','mu2','sigma2'}}...
);

fitmodels(end+1) = struct(...
    'Name','2x Gaussian + BG',...
    'FitFunction',@fitNGaussian,...
    'FitArguments',{{2,inlcudeBGcomponent}},...
    'FitParameter',{{'amp1','mu1','sigma1','amp2','mu2','sigma2','bg'}},...
    'FitInitParameter',{{'mu1','sigma1','mu2','sigma2'}}...
);

fitmodels(end+1) = struct(...
    'Name','2x Gaussian',...
    'FitFunction',@fitNGaussian,...
    'FitArguments',{{2,~inlcudeBGcomponent}},...
    'FitParameter',{{'amp1','mu1','sigma1','amp2','mu2','sigma2'}},...
    'FitInitParameter',{{'mu1','sigma1','mu2','sigma2'}}...
);

fitmodels(end+1) = struct(...
    'Name','2x Gaussian + BG',...
    'FitFunction',@fitNGaussian,...
    'FitArguments',{{2,inlcudeBGcomponent}},...
    'FitParameter',{{'amp1','mu1','sigma1','amp2','mu2','sigma2','bg'}},...
    'FitInitParameter',{{'mu1','sigma1','mu2','sigma2'}}...
);

fitmodels(end+1) = struct(...
    'Name','3x Gaussian',...
    'FitFunction',@fitNGaussian,...
    'FitArguments',{{3,~inlcudeBGcomponent}},...
    'FitParameter',{{'amp1','mu1','sigma1','amp2','mu2','sigma2','amp3','mu3','sigma3'}},...
    'FitInitParameter',{{'mu1','sigma1','mu2','sigma2','mu3','sigma3'}}...
);

fitmodels(end+1) = struct(...
    'Name','3x Gaussian + BG',...
    'FitFunction',@fitNGaussian,...
    'FitArguments',{{3,inlcudeBGcomponent}},...
    'FitParameter',{{'amp1','mu1','sigma1','amp2','mu2','sigma2','bg'}},...
    'FitInitParameter',{{'mu1','sigma1','mu2','sigma2','mu3','sigma3'}}...
);


fitmodels(end+1) = struct(...
    'Name','1x Exp',...
    'FitFunction',@fitNExp,...
    'FitArguments',{{1,~inlcudeBGcomponent}},...
    'FitParameter',{{'amp','tau'}},...
    'FitInitParameter',{{'tau'}}...
);
fitmodels(end+1) = struct(...
    'Name','1x Exp + BG',...
    'FitFunction',@fitNExp,...
    'FitArguments',{{1,inlcudeBGcomponent}},...
    'FitParameter',{{'amp','tau'}},...
    'FitInitParameter',{{'tau'}}...
);
fitmodels(end+1) = struct(...
    'Name','2x Exp',...
    'FitFunction',@fitNExp,...
    'FitArguments',{{2,~inlcudeBGcomponent}},...
    'FitParameter',{{'amp','tau'}},...
    'FitInitParameter',{{'tau'}}...
);
fitmodels(end+1) = struct(...
    'Name','2x Exp + BG',...
    'FitFunction',@fitNExp,...
    'FitArguments',{{2,inlcudeBGcomponent}},...
    'FitParameter',{{'amp','tau'}},...
    'FitInitParameter',{{'tau'}}...
);
fitmodels(end+1) = struct(...
    'Name','3x Exp',...
    'FitFunction',@fitNExp,...
    'FitArguments',{{3,~inlcudeBGcomponent}},...
    'FitParameter',{{'amp','tau'}},...
    'FitInitParameter',{{'tau'}}...
);
fitmodels(end+1) = struct(...
    'Name','3x Exp + BG',...
    'FitFunction',@fitNExp,...
    'FitArguments',{{3,inlcudeBGcomponent}},...
    'FitParameter',{{'amp','tau'}},...
    'FitInitParameter',{{'tau'}}...
);
%%
if nargin==0
    varargout{1} = {fitmodels.Name};
    varargout{2} = fitmodels;
    return
end
%%
if ~exist('nModel','var') || isempty(nModel)
    nModel = 1;
end
assert(nModel>0 && nModel<=numel(fitmodels));

if ~exist('binedges','var') || isempty(binedges)
    binedges = gca;
end
% if handle to histogram is passed get bins and values
if isgraphics(binedges)
    h = findobj(binedges,'Type','Histogram');
    if isempty(h)
        h = findobj(binedges.Parent,'Type','Histogram');
    end
    if isempty(h)
        error('No histogram passed or found.');
    else
        h = h(1);
    end
    
    binedges = h.BinEdges;
    binvalues = h.Values;
end
if ~exist('initValues','var')
    initValues = [];
end

[p,initValues,z,p_text,bincenter_fine,z_fine] = fitmodels(nModel).FitFunction(binedges,binvalues,initValues,fitmodels(nModel).FitArguments{:});

varargout = {p,initValues,z,p_text,bincenter_fine,z_fine};

end

function [p,initValues,z,p_text,bincenter_fine,z_fine] = noFit(binedges,binvalues,initValues)
    [p,initValues,z,p_text,bincenter_fine,z_fine] = deal([]);
end

function [p,initValues,z,p_text,bincenter_fine,z_fine] = fitNGaussian(binedges,binvalues,initValues,nGaussians,bgFlag)
    bincenter = (binedges(1:end-1) + binedges(2:end))/2;
    ind = binvalues>0.05*median(binvalues(binvalues>mean(binvalues))); % exclude background from extimation
    hsum = sum(binvalues(ind));
    hmean = sum(binvalues(ind).*bincenter(ind))./hsum;
    hstd = sqrt(abs(sum(binvalues(ind).*bincenter(ind).^2)-(sum(binvalues(ind).*bincenter(ind))).^2/hsum)/hsum)+eps; % +eps ensures sigma>0

    %%
    if isempty(initValues)
        peakpos = findPeakPos(binvalues,nGaussians,hstd/mean(diff(bincenter))/2);
        if isempty(peakpos)
            attmpt_mod = ((1:nGaussians)-nGaussians/2-0.5);
            peakpos = hmean+attmpt_mod*hstd;
        else
            peakpos = interp1(1:numel(bincenter),bincenter,peakpos);
        end
        initValues = reshape([peakpos(:)';ones(1,nGaussians)*hstd/nGaussians],[],1);  
    end
    %% LSQnonneg fit to guess amplitudes
    f0 = inf;
    p0 = initValues;
    for idx = 1:10
        [p,~,~,f] = Simplex_Handle(@nGaussianLSQ,p0,[],[],[],[],binedges(:),binvalues(:),bgFlag);
        if f<f0
            f0 = f;
            p0 = p;
        end
    end
    initValues = p0;
    [~,c] = nGaussianLSQ(p,binedges(:),binvalues(:),bgFlag);
    %% full MLE fit
    p0 = [reshape([c(1:nGaussians)';reshape(p,[],nGaussians)],[],1);c(nGaussians+1:end)];
    pmin = [repmat([0; -inf; 0],[nGaussians 1]);0*c(nGaussians+1:end)];
    f0 = inf;
    for idx = 1:10
        [p,~,~,f] = Simplex_Handle(@nGaussianFull,p0,pmin,[],[],[],binedges(:),binvalues(:),bgFlag);
        if f<f0
            f0 = f;
            p0 = p;
        end
    end
    [~,~,~,z] = nGaussianFull(p,binedges(:),binvalues(:),bgFlag);
    %% generate output text
    if nargout > 3
        p_text = sprintf('\n%.4g \\pm %.4g (%2.f)',[p(2:3:end-1),p(3:3:end),p(1:3:end-1)]');
        if bgFlag
            p_text = [p_text sprintf('\nBG (%2.f)',p(end))];
        end
    end
    %% divide bins for smoother plot
    if nargout > 4
        nDiv = 10;
        binedges_fine = [reshape(binedges(1:end-1)+(linspace(0,1-1/nDiv,nDiv))'*diff(binedges),[],1);binedges(end)];
        bincenter_fine = (binedges_fine(1:end-1) + binedges_fine(2:end))/2;

        [~,~,~,z_fine] = nGaussianFull(p,binedges_fine(:),[],bgFlag);
        z_fine = nDiv*z_fine;
    end
end

function [p,initValues,z,p_text,bincenter_fine,z_fine] = fitNExp(binedges,binvalues,initValues,nExp,bgFlag)
    bincenter = (binedges(1:end-1) + binedges(2:end))/2;
    ind = binvalues>0.05*median(binvalues(binvalues>mean(binvalues))); % exclude background from extimation
    hsum = sum(binvalues(ind));
    hmean = sum(binvalues(ind).*bincenter(ind))./hsum;
    hstd = sqrt(abs(sum(binvalues(ind).*bincenter(ind).^2)-(sum(binvalues(ind).*bincenter(ind))).^2/hsum)/hsum)+eps; % +eps ensures sigma>0

    %%
    if isempty(initValues)
        attmpt_mod = ((1:nExp)-nExp/2-0.5);
        initValues = hstd+attmpt_mod*hstd;
    end
    %% LSQnonneg fit to guess amplitudes    
    f0 = inf;
    p0 = initValues;
    for idx = 1:10
        [p,~,~,f] = Simplex_Handle(@nExpLSQ,p0,[],[],[],[],binedges(:),binvalues(:),bgFlag);
        if f<f0
            f0 = f;
            p0 = p;
        end
    end
    initValues = p0;
    [~,c] = nExpLSQ(p,binedges(:),binvalues(:),bgFlag);
    %% full MLE fit
    p0 = [reshape([c(1:nExp)';reshape(p,[],nExp)],[],1);c(nExp+1:end)];
    pmin = [repmat([0; -inf],[nExp 1]);0*c(nExp+1:end)];
    f0 = inf;
    for idx = 1:10
        [p,~,~,f] = Simplex_Handle(@nExpFull,p0,pmin,[],[],[],binedges(:),binvalues(:),bgFlag);
        if f<f0
            f0 = f;
            p0 = p;
        end
    end
    [~,~,~,z] = nExpFull(p,binedges(:),binvalues(:),bgFlag);
    %% generate output text
    if nargout > 3
        p_text = [];
        p_text = sprintf('\n%.4g (%2.f)',[p(2:2:end),p(1:2:end-1)]');
        if bgFlag
            p_text = [p_text sprintf('\nBG (%2.f)',p(end))];
        end
    end
    %% divide bins for smoother plot
    if nargout > 4
        nDiv = 10;
        binedges_fine = [reshape(binedges(1:end-1)+(linspace(0,1-1/nDiv,nDiv))'*diff(binedges),[],1);binedges(end)];
        bincenter_fine = (binedges_fine(1:end-1) + binedges_fine(2:end))/2;

        [~,~,~,z_fine] = nExpFull(p,binedges_fine(:),[],bgFlag);
        z_fine = nDiv*z_fine;
    end
end

function peakpos = findPeakPos(values,n,hsmooth)
    if nargin<3
        hsmooth = numel(values)/10;
    end
    testvalues = linspace(0,max(values));
    peakedges = diff((movmean([0;values(:);0],ceil(hsmooth))>testvalues));
    peakedges = peakedges.*(abs(movmean(peakedges,3))>0.9/3);% The edges need to be 3 bins apart
    ind = find(sum(peakedges==1,1)==n & sum(peakedges==-1,1)==n,1,'last');
    if ~isempty(ind)
        peakpos = (find(peakedges(:,ind)==1)+find(peakedges(:,ind)==-1))/2;            
    else
        peakpos = [];
    end
end

function [err,c,zz,z] = nGaussianLSQ(p,edges,values,bgFlag)
    % p = [mu1 sigma1 mu2 sigma2]
    values = values(:);
    edges = edges(:);
    p = p(:)';
    if nargin<4 || isempty(bgFlag)
        bgFlag = false;
    end

    distcdf = @(x,mu,sigma)-0.5*erfc((x-mu)./sqrt(2.*sigma.^2)); %erfc(x) = 1-erf(x), should be more precise. The constant offset gets cancled in the diffrence (next line);
    distfun = @(edges,mu,sigma,amp)(distcdf(edges(2:end),mu(:)',sigma(:)')-distcdf(edges(1:end-1),mu(:)',sigma(:)'));

    zz = distfun(edges(:),p(1:2:end),p(2:2:end));
    if bgFlag
        zz = [zz, (1/size(zz,1))*ones(size(zz,1),1)];
    end
    if numel(values)==size(zz,2)
        c = values;
        z = zz*c;
        err = NaN;
    else
        c = lsqnonneg(zz,values);
        z = zz*c;
        % MLE for Poission https://www.nature.com/articles/nmeth0510-338
        ind = values>0;
        err = 2*sum(z-values)-2*sum(values(ind).*log(z(ind)./values(ind))); % assuming histogram counts follow Poissonian distribution
    end
end

function [err,c,zz,z] = nGaussianFull(p,edges,values,bgFlag)
    % p = [amp1 mu1 sigma1 amp2 mu2 sigma2 bg]
    values = values(:);
    edges = edges(:);
    p = p(:)';
    if nargin<4 || isempty(bgFlag)
        bgFlag = rem(numel(p),3);
    end

    distcdf = @(x,mu,sigma)-0.5*erfc((x-mu)./sqrt(2.*sigma.^2)); %erfc(x) = 1-erf(x), should be more precise. The constant offset gets cancled in the diffrence (next line);
    distfun = @(edges,mu,sigma,amp)(distcdf(edges(2:end),mu(:)',sigma(:)')-distcdf(edges(1:end-1),mu(:)',sigma(:)'));

    zz = distfun(edges(:),p(2:3:end),p(3:3:end));
    if bgFlag
        zz = [zz, (1/size(zz,1))*ones(size(zz,1),1)];
    end
    c = p(1:3:end)';
    z = zz*c;
    if nargin<3 || isempty(values)
        err = NaN;
        return;
    end
    % MLE for Poission https://www.nature.com/articles/nmeth0510-338
    ind = values>0;
    err = 2*sum(z-values)-2*sum(values(ind).*log(z(ind)./values(ind))); % assuming histogram counts follow Poissonian distribution
end

function [err,c,zz,z] = nExpLSQ(p,edges,values,bgFlag)
    % p = [mu1 sigma1 mu2 sigma2]
    values = values(:);
    edges = edges(:);
    p = p(:)';
    if nargin<4 || isempty(bgFlag)
        bgFlag = false;
    end

    distcdf = @(x,tau)(1-exp(-x./tau)); 
    distfun = @(edges,tau,amp)(distcdf(edges(2:end),tau(:)')-distcdf(edges(1:end-1),tau(:)'));

    zz = distfun(edges(:),p);
    if bgFlag
        zz = [zz, (1/size(zz,1))*ones(size(zz,1),1)];
    end
    if numel(values)==size(zz,2)
        c = values;
        z = zz*c;
        err = NaN;
    else
        c = lsqnonneg(zz,values);
        z = zz*c;
        % MLE for Poission https://www.nature.com/articles/nmeth0510-338
        ind = values>0;
        err = 2*sum(z-values)-2*sum(values(ind).*log(z(ind)./values(ind))); % assuming histogram counts follow Poissonian distribution
    end
end

function [err,c,zz,z] = nExpFull(p,edges,values,bgFlag)
    % p = [amp1 tau1 amp2 tau2 bg]
    values = values(:);
    edges = edges(:);
    p = p(:)';
    if nargin<4 || isempty(bgFlag)
        bgFlag = rem(numel(p),2);
    end

    distcdf = @(x,tau)(1-exp(-x./tau));
    distfun = @(edges,tau,amp)(distcdf(edges(2:end),tau(:)')-distcdf(edges(1:end-1),tau(:)'));

    zz = distfun(edges(:),p(2:2:end));
    if bgFlag
        zz = [zz, (1/size(zz,1))*ones(size(zz,1),1)];
    end
    c = p(1:2:end)';
    z = zz*c;
    if nargin<3 || isempty(values)
        err = NaN;
        return;
    end
    % MLE for Poission https://www.nature.com/articles/nmeth0510-338
    ind = values>0;
    err = 2*sum(z-values)-2*sum(values(ind).*log(z(ind)./values(ind))); % assuming histogram counts follow Poissonian distribution
end