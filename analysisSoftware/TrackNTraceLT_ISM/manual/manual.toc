\babel@toc {american}{}
\contentsline {section}{\numberline {1}Installation}{3}{section.0.1}%
\contentsline {subsection}{\numberline {1.1}Windows}{3}{subsection.0.1.1}%
\contentsline {subsection}{\numberline {1.2}Linux (tested with Kubuntu 14.4)}{3}{subsection.0.1.2}%
\contentsline {section}{\numberline {2}Overview}{4}{section.0.2}%
\contentsline {subsection}{\numberline {2.1}Startup (Fig. \ref {fig:flowdiagram}.1)}{4}{subsection.0.2.1}%
\contentsline {subsection}{\numberline {2.2}Main GUI (Fig. \ref {fig:flowdiagram}.2)}{5}{subsection.0.2.2}%
\contentsline {subsection}{\numberline {2.3}Preview/Visualizer (Fig. \ref {fig:flowdiagram}.3)}{6}{subsection.0.2.3}%
\contentsline {paragraph}{Player options}{6}{section*.5}%
\contentsline {paragraph}{Histogram options (unavailable in \textsf {Movie} mode)}{7}{section*.6}%
\contentsline {paragraph}{Tracking related (only \textsf {Tracking} mode)}{7}{section*.7}%
\contentsline {section}{\numberline {3}TrackNTrace output}{8}{section.0.3}%
\contentsline {section}{\numberline {4}How to write a TrackNTrace plugin}{9}{section.0.4}%
\contentsline {subsection}{\numberline {4.1}Plugin header}{9}{subsection.0.4.1}%
\contentsline {subsection}{\numberline {4.2}Plugin body}{11}{subsection.0.4.2}%
\contentsline {subsection}{\numberline {4.3}Optional pre- and post-processing functions}{12}{subsection.0.4.3}%
\contentsline {subsection}{\numberline {4.4}Global variables and parallelization}{12}{subsection.0.4.4}%
\contentsline {section}{\numberline {5}References}{14}{section.0.5}%
