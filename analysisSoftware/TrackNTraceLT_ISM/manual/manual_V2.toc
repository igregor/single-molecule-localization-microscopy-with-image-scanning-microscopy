\babel@toc {american}{}
\contentsline {section}{\numberline {1}Installation}{3}{section.0.1}%
\contentsline {subsection}{\numberline {1.1}Windows}{3}{subsection.0.1.1}%
\contentsline {subsection}{\numberline {1.2}Linux (tested with Kubuntu 14.4)}{3}{subsection.0.1.2}%
\contentsline {section}{\numberline {2}Overview}{4}{section.0.2}%
\contentsline {subsection}{\numberline {2.1}Startup (Fig. \ref {fig:flowdiagram}.1)}{4}{subsection.0.2.1}%
\contentsline {subsection}{\numberline {2.2}Main GUI (Fig. \ref {fig:flowdiagram}.2)}{5}{subsection.0.2.2}%
\contentsline {subsection}{\numberline {2.3}Preview/Visualizer (Fig. \ref {fig:flowdiagram}.3)}{6}{subsection.0.2.3}%
\contentsline {paragraph}{Player options}{7}{section*.5}%
\contentsline {paragraph}{Histogram}{7}{section*.6}%
\contentsline {paragraph}{Tracks (only affects \textsf {Tracking} mode)}{8}{section*.7}%
\contentsline {paragraph}{Filter}{8}{section*.8}%
\contentsline {paragraph}{Reconstruct}{8}{section*.9}%
\contentsline {paragraph}{Drift}{9}{section*.10}%
\contentsline {subsection}{\numberline {2.4}Headless mode}{10}{subsection.0.2.4}%
\contentsline {section}{\numberline {3}TrackNTrace output}{11}{section.0.3}%
\contentsline {section}{\numberline {4}How to write a TrackNTrace plugin}{12}{section.0.4}%
\contentsline {subsection}{\numberline {4.1}Plugin header}{12}{subsection.0.4.1}%
\contentsline {subsection}{\numberline {4.2}Plugin body}{14}{subsection.0.4.2}%
\contentsline {subsection}{\numberline {4.3}Optional pre- and post-processing functions}{15}{subsection.0.4.3}%
\contentsline {subsection}{\numberline {4.4}Global variables and parallelization}{16}{subsection.0.4.4}%
\contentsline {subsection}{\numberline {4.5}File import plugins}{17}{subsection.0.4.5}%
\contentsline {section}{\numberline {5}Provided TrackNTrace plugins (selection)}{18}{section.0.5}%
\contentsline {subsection}{\numberline {5.1}File import plugins}{18}{subsection.0.5.1}%
\contentsline {paragraph}{TIFF (*.tif;*.tiff)}{18}{section*.13}%
\contentsline {paragraph}{PTU (PicoQuant Unified TTTR) single photon files (*.ptu)}{18}{section*.14}%
\contentsline {subsection}{\numberline {5.2}Candidate detection plugins}{18}{subsection.0.5.2}%
\contentsline {paragraph}{Cross correlation}{18}{section*.15}%
\contentsline {paragraph}{Wavelet filtering}{19}{section*.16}%
\contentsline {paragraph}{Import CSV}{20}{section*.17}%
\contentsline {subsection}{\numberline {5.3}Refinement plugins}{20}{subsection.0.5.3}%
\contentsline {paragraph}{TNT Fitter}{20}{section*.18}%
\contentsline {subsection}{\numberline {5.4}Tracking plugins}{20}{subsection.0.5.4}%
\contentsline {paragraph}{TNT NearestNeighbor}{20}{section*.19}%
\contentsline {subsection}{\numberline {5.5}Post-processing plugins}{21}{subsection.0.5.5}%
\contentsline {paragraph}{fitLT}{21}{section*.20}%
\contentsline {section}{\numberline {6}References}{23}{section.0.6}%
