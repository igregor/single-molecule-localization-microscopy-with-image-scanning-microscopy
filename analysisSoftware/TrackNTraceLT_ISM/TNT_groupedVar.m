function [fig] = TNT_histogram(TNT_locData,TNT_metaData)
if nargin==0
    try
        TNT_locData = evalin('base','TNT_locData');
        TNT_metaData = evalin('base','TNT_metaData');
    catch
        
    end
end
if ~exist('TNT_locData','var') || isempty(TNT_locData)
    warning('First input argument or workspace variable TNT_locData needs to be a table.');
    fig = gobjects(0);
    return
end

if ~istable(TNT_locData)
    if isnumeric(TNT_locData)
        if nargin>1 && iscellstr(TNT_metaData) && numel(TNT_metaData)==size(TNT_locData,2)
            varNames = TNT_metaData;
        else
            varNames = arrayfun(@(n)sprintf('col%g',n),1:size(TNT_locData,2),'UniformOutput',false);
        end
        TNT_locData = array2table(TNT_locData,'VariableNames',varNames);
    else
        warning('Unsupported data type: TNT_locData needs to be a table or matrix.');        
    end
end

%%
hist_avgTrack = true;
hist_nbins = 20;
hist_param = NaN;
hist_hdl = gobjects(1);
fit_type = 1;
fit_initValues = [];
plot_mode = 1;

data_track_ind = find(contains(TNT_locData.Properties.VariableNames,'track_ID','IgnoreCase',true),1);
hist_gparam = find(contains(TNT_locData.Properties.VariableNames,{'nphoton','Amp_Peak_'},'IgnoreCase',true),1,'last');
if isempty(hist_gparam)
    hist_gparam = 1;
end
%%
% pixelSize = TNT_metaData.pixelsize * 1e3;
fig = figure('Name','TNT Grouped analysis');
phdl = uipanel(fig,'Position',[0.01 0.01 0.98 0.9],'BackgroundColor',[0.98 0.98 0.98]);



popup_parameter = uicontrol('Style','popupmenu','String',[TNT_locData.Properties.VariableNames {'Overview'}],'Value',numel(TNT_locData.Properties.VariableNames)+1,...
                    'Units','normalized','Position',[0.01 0.93 0.20 0.05]);
prev_ui = popup_parameter;
check_avgTracks = uicontrol('Style','checkbox','String','avgTracks','Value',hist_avgTrack,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.13  prev_ui.Position(4)]);
if isempty(data_track_ind)
    hist_avgTrack = false;
    check_avgTracks.Value = hist_avgTrack;
    check_avgTracks.Enable = 'off';
end
prev_ui = check_avgTracks;
popup_gparameter = uicontrol('Style','popupmenu','String',TNT_locData.Properties.VariableNames,'Value',hist_gparam,...
                    'Units','normalized','Position',[sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.20  prev_ui.Position(4)]);
prev_ui = popup_gparameter;
edit_nbins = uicontrol('Style','edit','String',sprintf('%g',hist_nbins),'Callback',{@callback_FloatEdit,1,[],'','%.0f'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = edit_nbins;
popup_plotmode = uicontrol('Style','popupmenu','String',{'mean, std','mean +/- std','25-50-75 percentile'},'Value',plot_mode,...
                    'Units','normalized','Position',[sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.26  prev_ui.Position(4)]);
prev_ui = popup_plotmode;

button_update = uicontrol('Style','pushbutton','String','Update','Callback',@callback_updatePlot,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.09  prev_ui.Position(4)]);
%%
callback_updatePlot();

    function callback_updateData(TNT_locData_new,TNT_metaData_new)
        switch nargin
            case 0
                TNT_locData = evalin('base','TNT_locData');
                TNT_metaData = evalin('base','TNT_metaData');
            case 1
                TNT_locData = TNT_locData_new;
            case 2
                TNT_locData = TNT_locData_new;
                TNT_metaData = TNT_metaData_new;
        end
    end

    function callback_updatePlot(~,~)
        hist_avgTrack = check_avgTracks.Value;
        hist_nbins = str2double(edit_nbins.String);
        hist_param = popup_parameter.Value;
        hist_gparam = popup_gparameter.Value;
        plot_mode = popup_plotmode.Value;
        delete(phdl.Children);
        
        if hist_param>numel(TNT_locData.Properties.VariableNames)
            plotOverview();
        else
            hist_hdl = plotParameter(axes(phdl),hist_param,hist_gparam);
        end
    end
    function hist_obj = plotParameter(ax,pidx,gidx)
        if hist_avgTrack
            [~,~,idx] = unique(TNT_locData{:,data_track_ind});
            % faster than @mean argument
            pcount = accumarray(idx,1); 
            pdata = accumarray(idx,TNT_locData{:,pidx});
            pdata(pcount>0) = pdata./pcount;
            gdata = accumarray(idx,TNT_locData{:,gidx});
            gdata(pcount>0) = gdata./pcount;
        else
            pdata = TNT_locData{:,pidx};
            gdata = TNT_locData{:,gidx};
        end
        ind = ~isnan(pdata) & ~isnan(gdata);
        nbins = min(numel(unique(pdata(ind))),hist_nbins);
        showtitle = ~isgraphics(ax.Parent,'uipanel');
        if ~isempty(ind) && nbins>1
            [gdata_s,ord] = sort(gdata(ind));
            pdata = pdata(ind);
            pdata_s = pdata(ord);

            q_ind = ceil(linspace(eps,1,numel(pdata))*nbins)';
        
            q_std = accumarray(q_ind,pdata_s,[nbins,1],@std);
            q_mean = accumarray(q_ind,pdata_s,[nbins,1],@mean);
            q_gmean = accumarray(q_ind,gdata_s,[nbins,1],@mean);
            
            %%
            switch plot_mode
                case 1
                    if ~showtitle
                        axm = subplot(2,1,1,ax);
                        hist_obj = plot(axm,q_gmean,q_mean);
                        axs = subplot(2,1,2,copyobj(axm,axm.Parent));
                        plot(axs,q_gmean,q_std);
                        
                        axs.XLabel.Interpreter = 'none';
                        axs.XLabel.String = TNT_locData.Properties.VariableNames{gidx};
                        
                        axm.YLabel.Interpreter = 'none';
                        axm.YLabel.String = ['mean(' TNT_locData.Properties.VariableNames{pidx} ')'];
                        axs.YLabel.Interpreter = 'none';
                        axs.YLabel.String = ['std(' TNT_locData.Properties.VariableNames{pidx} ')'];
                        linkaxes([axs axm],'x');
                        
                    else
                        yyaxis(ax,'left');
                        hist_obj = plot(ax,q_gmean,q_mean);
                        ax.YLabel.String = 'mean';
                        yyaxis(ax,'right');
                        plot(ax,q_gmean,q_std);
                        ax.YLabel.String = 'std';
                        
                        ax.XLabel.String = TNT_locData.Properties.VariableNames{gidx};
                        ax.XLabel.Interpreter = 'none';
                    end
                    
                case 2
                    hist_obj = plot(ax,q_gmean,q_mean,'.-');
                    ax.NextPlot = 'add';
                    uistack(fill(ax,[q_gmean;flip(q_gmean)],[q_mean(:)+q_std(:);flip(q_mean(:)-q_std(:))],hist_obj.Color,'EdgeColor','none','FaceAlpha',0.4),'bottom');
                    
                    ax.XLabel.Interpreter = 'none';
                    ax.YLabel.Interpreter = 'none';
                    
                    ax.XLabel.String = TNT_locData.Properties.VariableNames{gidx};
                    ax.YLabel.String = TNT_locData.Properties.VariableNames{pidx};
                    
                case 3
                    q_quant = accumarray(q_ind,pdata_s,[nbins,1],@(x){quantile(x(:),[0.25 0.5 0.75])});
                    q_quant = vertcat(q_quant{:});
                    q_gmean = accumarray(q_ind,gdata_s,[nbins,1],@median);
                    
                    hist_obj = plot(ax,q_gmean,q_quant(:,2),'.-');
                    ax.NextPlot = 'add';
                    uistack(fill(ax,[q_gmean;flip(q_gmean)],[q_quant(:,1);flip(q_quant(:,3))],hist_obj.Color,'EdgeColor','none','FaceAlpha',0.4),'bottom');
                    
                    ax.XLabel.Interpreter = 'none';
                    ax.YLabel.Interpreter = 'none';
                    
                    ax.XLabel.String = TNT_locData.Properties.VariableNames{gidx};
                    ax.YLabel.String = TNT_locData.Properties.VariableNames{pidx};       
            end
        end
        if showtitle
            title(TNT_locData.Properties.VariableNames{pidx},'Interpreter','none');
        end
    end
    function plotOverview(~,~)
        thdl = tiledlayout(phdl,'flow');
        for pidx = 1:size(TNT_locData,2)
            plotParameter(nexttile(thdl),pidx,hist_gparam);
        end
    end
end
% Callback for edit fields containing floats. Checks if a correct
% number was entered and restricts it to the given bounds.
    function callback_FloatEdit(hObj,~, minVal, maxVal, flag, formatstring)
        if nargin<3 || isempty(minVal)
            minVal=-inf;
        end
        if nargin<4 || isempty(maxVal)
            maxVal=inf;
        end
        if nargin<6 || isempty(formatstring)
            formatstring='%.2f';
        end
        
        value = str2num(get(hObj, 'String')); %#ok<ST2NM> str2num distinguishes NaN and invalid input, str2double does not.
        if isempty(value)
            set(hObj,'ForegroundColor','r');
            set(hObj,'String','INVALID');
            uicontrol(hObj);
        elseif nargin>4 && isnan(value) && ischar(flag) && strcmpi(flag,'includeNaN')
            set(hObj,'ForegroundColor','k');
            set(hObj,'String','NaN');
        else
            value = max(minVal,value);
            value = min(maxVal,value);
            set(hObj,'ForegroundColor','k');
            set(hObj,'String',sprintf(formatstring,value));
        end
    end