function TNT_profile(TNT_locData,TNT_metaData)
if nargin==0
    try
        TNT_locData = evalin('base','TNT_locData');
        TNT_metaData = evalin('base','TNT_metaData');
    catch
        
    end
end
if ~exist('TNT_locData','var') || isempty(TNT_locData)
    warning('First input argument or workspace variable TNT_locData needs to be a table.');
    fig = gobjects(0);
    return
end

if ~istable(TNT_locData)
    if isnumeric(TNT_locData)
        if nargin>1 && iscellstr(TNT_metaData) && numel(TNT_metaData)==size(TNT_locData,2)
            varNames = TNT_metaData;
        else
            varNames = arrayfun(@(n)sprintf('col%g',n),1:size(TNT_locData,2),'UniformOutput',false);
        end
        TNT_locData = array2table(TNT_locData,'VariableNames',varNames);
    else
        warning('Unsupported data type: TNT_locData needs to be a table or matrix.');        
    end
end

pixelSize = TNT_metaData.pixelsize * 1e3;
profile_width = 200; %nm
profile_sigma = 10; %nm
profile_hdl = gobjects(1);
profile_loi = gobjects(1);
profile_patch = gobjects(1);
profile_param = NaN;
profile_avgTrack = true;

image_superResMag = 20;
image_reconstruction = [];
image_kern = [];
valid_loc = [];

data_track_ind = find(contains(TNT_locData.Properties.VariableNames,'track_ID','IgnoreCase',true),1);

%% UI

fig = figure('Name','TNT Profile');
phdl = uipanel(fig,'Position',[0.01 0.01 0.98 0.9],'BackgroundColor',[0.98 0.98 0.98]);



popup_parameter = uicontrol('Style','popupmenu','String',[TNT_locData.Properties.VariableNames {'None'}],'Value',numel(TNT_locData.Properties.VariableNames)+1,...
                    'Units','normalized','Position',[0.01 0.935 0.18 0.045]);
prev_ui = popup_parameter;
check_avgTracks = uicontrol('Style','checkbox','String','avgTracks','Value',profile_avgTrack,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.13  prev_ui.Position(4)]);
if isempty(data_track_ind)
    profile_avgTrack = false;
    check_avgTracks.Value = profile_avgTrack;
    check_avgTracks.Enable = 'off';
end
prev_ui = check_avgTracks;
edit_width = uicontrol('Style','edit','String',sprintf('%g',profile_width),'Callback',{@callback_FloatEdit,[],[],[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.08  prev_ui.Position(4)]);              
prev_ui = edit_width;
text_width = uicontrol('Style','text','String','width (nm)',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.1  prev_ui.Position(4)]);              
prev_ui = text_width;
edit_sigma = uicontrol('Style','edit','String',sprintf('%g',profile_sigma),'Callback',{@callback_FloatEdit,[],[],[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.08  prev_ui.Position(4)]);              
prev_ui = edit_sigma;
text_sigma = uicontrol('Style','text','String','sigma (nm)',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.1  prev_ui.Position(4)]);              
prev_ui = text_sigma;

button_update = uicontrol('Style','pushbutton','String','Apply','Callback',@callback_update,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.1  prev_ui.Position(4)]);
prev_ui = button_update;         

button_update = uicontrol('Style','pushbutton','String','Export to WS','Callback',@callback_export,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.15  prev_ui.Position(4)]);

%%

data = struct();
warned = false;
callback_update([],[]);
%% Postproc data -> struct
% data = struct();
% data.lt = accumarray(TNT_locData.Track_ID,TNT_locData.lt_tau,[],@mean);
% data.np = accumarray(TNT_locData.Track_ID,TNT_locData.nphoton,[],@mean);
% data.x = accumarray(TNT_locData.Track_ID,TNT_locData.x,[],@mean);
% data.y = accumarray(TNT_locData.Track_ID,TNT_locData.y,[],@mean);
% data.chi2 = accumarray(TNT_locData.Track_ID,TNT_locData.lt_chi2,[],@mean);
% data.sigma = accumarray(TNT_locData.Track_ID,TNT_locData.sigma,[],@mean);
% data.frame = accumarray(TNT_locData.Track_ID,TNT_locData.Frame,[],@min);


function callback_update(~,~)
    %%
    if ~isgraphics(phdl)
        return;
    end
    %% Update parameter
    
    profile_sigma = str2double(edit_sigma.String);
    profile_width = str2double(edit_width.String);
    profile_avgTrack = check_avgTracks.Value;
    profile_param = popup_parameter.Value;
    if profile_param<1||profile_param>size(TNT_locData,2)
        profile_param = NaN;
    else
        profile_param = TNT_locData.Properties.VariableNames{profile_param};
    end
    
    if profile_avgTrack
        [~,~,idx] = unique(TNT_locData{:,data_track_ind});
        % faster than @mean argument
        pcount = accumarray(idx,1);
        for pidx = 1:size(TNT_locData,2)
            pname = TNT_locData.Properties.VariableNames{pidx};
            data.(pname) = accumarray(idx,TNT_locData.(pname));
            data.(pname)(pcount>0) = data.(pname)./pcount;
        end
    else
        data = TNT_locData;
    end
    
    
    %% Filter

    ind = data.nphoton>10;

    %
    valid_loc = 1/image_superResMag < data.x  ...
              & 1/image_superResMag < data.y  ...
              & ind;
    image_reconstruction = accumarray(round(image_superResMag.*[data.y(valid_loc), data.x(valid_loc)]),1);

    sigma = profile_sigma/pixelSize*image_superResMag;
    [jx,jy] = meshgrid(-2*sigma:2*sigma,-2*sigma:2*sigma);
    image_kern = exp(-(jx.^2+jy.^2)./2./sigma.^2);
    image_reconstruction = conv2(image_reconstruction,image_kern,'same');

    %% Make mask and filter localisations
    delete(phdl.Children);
    ax_img = subplot(1,2,1,axes(phdl));
    imagesc(ax_img,image_reconstruction);
    colormap(ax_img,cmap_heat);
    axis(ax_img,'image');
    axis(ax_img,'off');
    % plot(colIdx,rowIdx,'o','MarkerFaceColor','w','MarkerEdgeColor','b');
    caxis(ax_img,quantile(image_reconstruction(:),[0.80 0.999]));
    TNTscalebar(ax_img,'Pixelsize',pixelSize/image_superResMag,'Unit',' nm')

    hdl_loi = drawpolyline(ax_img);
    if ~isgraphics(hdl_loi)
        return
    end
    profile_loi = hdl_loi;
    profile_patch = patch(ax_img,nan,nan,nan);
    profile_patch.PickableParts  = 'none';
    profile_patch.FaceAlpha  = 0.2;

    lineradius = ceil(profile_width/2/pixelSize*image_superResMag);% nm -> px

    [jx,jy] = meshgrid(-lineradius:lineradius,-lineradius:lineradius);
    image_kern = ceil(sqrt(jx.^2/lineradius^2+jy.^2/lineradius^2))<2;
    mask = zeros(size(image_reconstruction));
    mask_ind = mask;


    ax_profile = subplot(1,2,2,axes(phdl));
    yyaxis(ax_profile, 'left');
    profile_hdl = plot(ax_profile,[1 2],[1 2]);
    ax_profile.XLabel.String = 'position (nm)';
    ax_profile.YLabel.String = 'intensity';

    updateMask(profile_loi,[]);

    % img = hist_xy;
    % img(imdilate(mask,ones((lineradius+0.5)*2))~=mask) = max(hist_xy(:));
    % subplot(2,1,1);
    % imagesc(img);
    % caxis(quantile(img(:),[0.8 0.995]));
    % axis image;




    %%
    addlistener(profile_loi,'MovingROI',@updateMask);
    addlistener(profile_loi,'ROIMoved',@updateMask);
end
%%
function locInMask = updateMask(loi,~)
    trace_len = sum(sqrt(sum(diff(loi.Position,1,1).^2,2)))/image_superResMag; % in px
    mask_ind_offset = 0;
    mask = zeros(size(image_reconstruction));
    if any(loi.Position>flip(size(mask)),'all') || any(loi.Position<0.5,'all')
            loi.Position = max(min(loi.Position,flip(size(mask))),0.5);
    end
    
    lineradius = ceil(profile_width/2/pixelSize*image_superResMag);% nm -> px
    
    patch_xy = [];
    
    for pidx = 2:size(loi.Position,1)
        dx=loi.Position(pidx,1)-loi.Position(pidx-1,1);
        dy=loi.Position(pidx,2)-loi.Position(pidx-1,2);
        mask(...
            sub2ind( size(mask),...
             round(linspace(loi.Position(pidx,2),loi.Position(pidx-1,2),ceil(1+sqrt(dx^2+dy^2)))),...
             round(linspace(loi.Position(pidx,1),loi.Position(pidx-1,1),ceil(1+sqrt(dx^2+dy^2))))...
            )) = mask_ind_offset + pixelSize/image_superResMag*linspace(eps,sqrt(dx^2+dy^2),ceil(1+sqrt(dx^2+dy^2)));  
        mask_ind_offset = pixelSize/image_superResMag*sqrt(dx^2+dy^2);
        
        kern2 = zeros(size(image_kern));
        dxp = lineradius/sqrt(1+dy^2/dx^2)*sign(dx);
        dyp = lineradius/sqrt(1+dx^2/dy^2)*sign(dy);
        kern2(...
            sub2ind( size(kern2),...
             (size(kern2,1)+1)/2 + round(linspace(+dxp,-dxp,ceil(1+2*sqrt(dxp^2+dyp^2)))),...
             (size(kern2,2)+1)/2 + round(linspace(-dyp,+dyp,ceil(1+2*sqrt(dxp^2+dyp^2))))...
            )) = 1;
        kern2 = imdilate(kern2,ones(2));% closes gaps at steps
        
        % genrate patch coordinates
        patch_xy(end+(1:4),:) = reshape(loi.Position((-1:0)+pidx,:)'+[dyp;-dxp].*shiftdim([-1 1],-1),2,[])';
    end
    patch_xy = [patch_xy((2:4:end)+[-1; 0],:); flip(patch_xy((4:4:end)+[-1; 0],:),1)];
    set(profile_patch,'XData', patch_xy(:,1),'YData', patch_xy(:,2),'FaceVertexCData',[1 1 1]);
    
    if size(loi.Position,1)>2 
        if ~warned
            warning('multi segment lines are not yet supported and may yield unexcpected results.');
            warned = true;
        end
    else
            warned = false;
    end
    mask_ind = conv2(mask,kern2,'same');
    mask = conv2(mask>0,kern2,'same');
    mask_ind(mask>0) = mask_ind(mask>0)./mask(mask>0);
    mask = mask>0;
    
    
    % Filter localisations in mask 
    ind_inmask = valid_loc;
    ind_inmask(valid_loc) = mask(sub2ind(size(mask),round(data.y(valid_loc)*image_superResMag),round(data.x(valid_loc)*image_superResMag)));
    ind_inmask = find(ind_inmask);

    if ~isempty(ind_inmask)
        ind_linpos = mask_ind(sub2ind(size(mask),round(data.y(ind_inmask)*image_superResMag),round(data.x(ind_inmask)*image_superResMag)));
        [lint,lpos] = histcountswdist(ind_linpos,[],profile_sigma,0:1:mask_ind_offset);
        set(profile_hdl(1),'XData',movmean(lpos,2,'Endpoints','discard'),'YData',lint);
        
        if ~isnan(profile_param)
            if numel(profile_hdl)<2
                yyaxis(profile_hdl.Parent, 'right');
                profile_hdl(2) = plot(profile_hdl.Parent,[1 2],[1 2]);
                profile_hdl(2).Parent.YAxis(2).Label.Interpreter = 'none';
            end
            ind = ~isnan(data.(profile_param)(ind_inmask));
            [lt,lpos] = histcountswdist(ind_linpos(ind),data.(profile_param)(ind_inmask(ind)),profile_sigma,0:1:mask_ind_offset);
            lt(lint>0) = lt(lint>0)./lint(lint>0);
            lt(lint<1/sqrt(2*pi)/profile_sigma*exp(-2)) = NaN; % nan outside two sigma interval
            set(profile_hdl(2),'XData',movmean(lpos,2,'Endpoints','discard'),'YData',lt,'Visible','on');
            profile_hdl(2).Parent.YAxis(2).Label.String = profile_param;
        elseif numel(profile_hdl)>1
            profile_hdl(2).Visible = 'off';
        end
    else
        set(profile_hdl(1),'XData',[0 mask_ind_offset],'YData',[0 0]);
        set(profile_hdl(end),'XData',[0 mask_ind_offset],'YData',[0 0]);
    end
    profile_hdl(1).Parent.XLim = [0 mask_ind_offset];
    
    if nargout>0
        data_table = struct2table(data);
        if ~isempty(ind_inmask)
            locInMask = data_table(ind_inmask,:);
            locInMask.profile_pos = ind_linpos;
        else
            locInMask = data_table([],:);
            locInMask.profile_pos = [];
        end
    end
end
function callback_export(~,~)
    if ~isgraphics(profile_loi)
        return
    end
    locInMask = updateMask(profile_loi,[]);
    
    outstruct = struct('position',profile_loi.Position/image_superResMag,'profile_axes',profile_hdl(1).XData,'profile_intensity',profile_hdl(1).YData,'profile_locData',locInMask);
    if ~isnan(profile_param)
        outstruct.(['profile_' profile_param]) = profile_hdl(2).YData;
    end
    assignin('base','TNT_profile_data',outstruct);
end
end

% Callback for edit fields containing floats. Checks if a correct
% number was entered and restricts it to the given bounds.
function callback_FloatEdit(hObj,~, minVal, maxVal, flag, formatstring)
    if nargin<3 || isempty(minVal)
        minVal=-inf;
    end
    if nargin<4 || isempty(maxVal)
        maxVal=inf;
    end
    if nargin<6 || isempty(formatstring)
        formatstring='%.2f';
    end

    value = str2num(get(hObj, 'String')); %#ok<ST2NM> str2num distinguishes NaN and invalid input, str2double does not.
    if isempty(value)
        set(hObj,'ForegroundColor','r');
        set(hObj,'String','INVALID');
        uicontrol(hObj);
    elseif nargin>4 && isnan(value) && ischar(flag) && strcmpi(flag,'includeNaN')
        set(hObj,'ForegroundColor','k');
        set(hObj,'String','NaN');
    else
        value = max(minVal,value);
        value = min(maxVal,value);
        set(hObj,'ForegroundColor','k');
        set(hObj,'String',sprintf(formatstring,value));
    end
end
    
function [n,edges,bin] = histcountswdist(x, w, sigma, varargin)
%HISTCOUNTSW Weighted Histogram Bin Counts
% [n,edges,bin] = histcountsw(x, w, sigma, varargin)
% Like histcounts but with an additional weighting of the data x
% with w. For w = 1 they give identical output.
% For sigma>0 a gaussian distribution with the width sigma is integrated
% over the bins and summed up for all x.
% Elements with the weight NaN are excluded.
% See HISTCOUNTS for more details.
%
% There are the following additional normaizaltion options:
%  'weights' (Default): Normalized to the weight of the counts.
%  'cumweight'        : Cummulative weights.
%  'weightdensity'    : Same as weights, but divded by the bin width.
% For the options 'count', 'cumcount' and 'countdensity' the weights are
% normalized to a mean of 1.
%
%
%
% Christoph Thiele, 2019

    if nargin<2 || isempty(w)
        w = 1;
    elseif ischar(w)
        if nargin>2
            varargin = [{w}, {sigma}, varargin];            
            sigma = [];
        else
            varargin = [{w}, varargin];
        end
        w = 1;
    end
    if nargin<3 || isempty(sigma)
        sigma = 0;
    elseif ischar(sigma)
        varargin = [{sigma}, varargin];
        sigma = 0;
    end
    
    ind = isnan(w) & isnan(sigma);
    if any(ind)
        w = w(~ind);
        x = x(~ind);
        sigma = sigma(~ind);
    end
%     norm = {'count', 'probability', 'countdensity', 'pdf', 'cumcount', 'cdf'};
    normidx = find(strcmpi(varargin,'Normalization'),1,'last');
    if ~isempty(normidx) 
        if numel(normidx)+1>numel(varargin)
            error(message('MATLAB:histcounts:ArgNameValueMismatch'));
        end
        norm = varargin{normidx+1};
        
        if contains(norm, 'weight')
            varargin{normidx+1} = strrep(norm,'weight','count');
        elseif contains(norm, 'count')
            w = w/sum(w(:))*numel(w);
        end
    else
        norm ='weights';
    end
    
    [n,edges,bin] = histcounts(x, varargin{:});
    
    % expand w if singleton
    if isscalar(w)
        w = w * ones(size(x));
    end    
    % expand sigma if singleton
    if isscalar(sigma)
        sigma = sigma * ones(size(x));
    end
    
    if all(sigma==0) % No distribution
        ind = bin~=0; % zero indicates values out of the bounds or NaNs.
        if any(ind)
            bin = bin(:);
            w = w(:);
            n = accumarray(bin(ind),w(ind),size(n'))';% sum up w for each bin.            
        end % else keep zeros from histcounts 
    else % distribution for each value
%         distcdf = @(x,mu,sigma)1/2*(1+erf((x-mu)./sqrt(2.*sigma.^2)));
        distcdf = @(x,mu,sigma)-0.5*erfc((x-mu)./sqrt(2.*sigma.^2)); %erfc(x) = 1-erf(x), should be more precise. The constant offset gets cancled in the diffrence (next line);
        distfun = @(edges,mu,sigma)distcdf(edges(2:end),mu,sigma)-distcdf(edges(1:end-1),mu,sigma);
        
        ind = ~isnan(x) & sigma>=0;
        if any(ind)
            sigma = max(sigma(:),eps);
            x = x(:);
            w = w(:);
            n = sum(w(ind).*distfun(edges(:)',x(ind),sigma(ind)),1);
        end % else keep zeros from histcounts 
    end
    switch norm
        case {'countdensity', 'weightdensity'}
            n = n./double(diff(edges));
        case {'cumcount', 'cumweight'}
            n = cumsum(n);
        case 'probability'
            n = n / sum(w);
        case 'pdf'
            n = n/sum(w)./double(diff(edges));
        case 'cdf'
            n = cumsum(n / sum(w));
    end
end

