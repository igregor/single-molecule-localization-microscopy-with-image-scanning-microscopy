function TNT_3Dplot(TNT_locData,TNT_metaData)
if nargin==0
    try
        TNT_locData = evalin('base','TNT_locData');
        TNT_metaData = evalin('base','TNT_metaData');
    catch
        
    end
end
if ~exist('TNT_locData','var') || isempty(TNT_locData)
    warning('First input argument or workspace variable TNT_locData needs to be a table.');
    fig = gobjects(0);
    return
end

if ~istable(TNT_locData)
    if isnumeric(TNT_locData)
        if nargin>1 && iscellstr(TNT_metaData) && numel(TNT_metaData)==size(TNT_locData,2)
            varNames = TNT_metaData;
        else
            varNames = arrayfun(@(n)sprintf('col%g',n),1:size(TNT_locData,2),'UniformOutput',false);
        end
        TNT_locData = array2table(TNT_locData,'VariableNames',varNames);
    else
        warning('Unsupported data type: TNT_locData needs to be a table or matrix.');        
    end
end
if isstruct(TNT_metaData)
    pixelSize_nm = TNT_metaData.pixelsize * 1e3;
else
    pixelSize_nm = 1;
end
profile_width = 200; %nm
profile_sigma = 10; %nm
profile_hdl = gobjects(1);
profile_loi = gobjects(1);
profile_patch = gobjects(1);
profile_param = NaN;
profile_avgTrack = true;

img_xlim = [nan nan];
img_ylim = [nan nan];
img_zlim = [nan nan];
img_clim = [nan nan];

image_superResMag = 20;
image_reconstruction = [];
image_kern = [];
valid_loc = [];

data_track_ind = find(contains(TNT_locData.Properties.VariableNames,'track_ID','IgnoreCase',true),1);

%% UI

fig = figure('Name','TNT 3D Plot');
phdl = uipanel(fig,'Position',[0.01 0.01 0.6 0.9],'BackgroundColor',[0.98 0.98 0.98]);
limitphdl = uipanel(fig,'Position',[0.61 0.01 0.38 0.9],'BackgroundColor',[0.98 0.98 0.98]);



popup_parameter = uicontrol('Style','popupmenu','String',[TNT_locData.Properties.VariableNames {'None'}],'Value',numel(TNT_locData.Properties.VariableNames)+1,...
                    'Units','normalized','Position',[0.01 0.935 0.18 0.045]);
prev_ui = popup_parameter;
check_avgTracks = uicontrol('Style','checkbox','String','avgTracks','Value',profile_avgTrack,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.13  prev_ui.Position(4)]);
if isempty(data_track_ind)
    profile_avgTrack = false;
    check_avgTracks.Value = profile_avgTrack;
    check_avgTracks.Enable = 'off';
end
prev_ui = check_avgTracks;
edit_width = uicontrol('Style','edit','String',sprintf('%g',profile_width),'Callback',{@callback_FloatEdit,[],[],[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.08  prev_ui.Position(4)]);              
prev_ui = edit_width;
text_width = uicontrol('Style','text','String','width (nm)',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.1  prev_ui.Position(4)]);              
prev_ui = text_width;
edit_sigma = uicontrol('Style','edit','String',sprintf('%g',profile_sigma),'Callback',{@callback_FloatEdit,[],[],[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.08  prev_ui.Position(4)]);              
prev_ui = edit_sigma;
text_sigma = uicontrol('Style','text','String','sigma (nm)',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.1  prev_ui.Position(4)]);              
prev_ui = text_sigma;

button_update = uicontrol('Style','pushbutton','String','Apply','Callback',@callback_update,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.1  prev_ui.Position(4)]);
prev_ui = button_update;         

button_update = uicontrol('Style','pushbutton','String','Export to WS','Callback',@callback_export,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.15  prev_ui.Position(4)]);

%%
loc_xyz_plot_nm = [pixelSize_nm*TNT_locData.('x'),pixelSize_nm*TNT_locData.('y'),TNT_locData.('z')];
% loc_xyz_plot_nm = loc_xyz_plot_nm - [1,1,0].*mean(loc_xyz_plot_nm,1);

img_xlim = quantile(loc_xyz_plot_nm(:,1),[0.01 0.99]);
img_ylim = quantile(loc_xyz_plot_nm(:,2),[0.01 0.99]);
img_zlim = quantile(loc_xyz_plot_nm(:,3),[0.01 0.99]);
img_clim = quantile(loc_xyz_plot_nm(:,3),[0.1 0.9]);


% scatter3(pixelSize_nm*data_tracked.x_ud(initInd), pixelSize_nm*data_tracked.y_ud(initInd), pixelSize_nm*data_tracked.z(initInd),20, pixelSize_nm*data_tracked.z(initInd),'o','MarkerEdgeAlpha',0.8)
delete(phdl.Children);
cax = axes(phdl);
% fig.Renderer = 'painters'; % looks better but is very slow
scatter3(cax,loc_xyz_plot_nm(:,1), loc_xyz_plot_nm(:,2), loc_xyz_plot_nm(:,3),5, loc_xyz_plot_nm(:,3),'o','MarkerFaceColor','flat','MarkerFaceAlpha',0.1,'MarkerEdgeAlpha',0);
% cax.NextPlot = 'add';
% scatter3(cax,loc_xyz_plot_nm(:,1), loc_xyz_plot_nm(:,2), loc_xyz_plot_nm(:,3),20, loc_xyz_plot_nm(:,3),'o','MarkerFaceColor','flat','MarkerFaceAlpha',0.2,'MarkerEdgeAlpha',0.1);
% scatter3(cax,loc_xyz_plot_nm(:,1), loc_xyz_plot_nm(:,2), loc_xyz_plot_nm(:,3),40, loc_xyz_plot_nm(:,3),'o','MarkerFaceColor','flat','MarkerFaceAlpha',0.2,'MarkerEdgeAlpha',0.1);

cax.XLim = img_xlim;
cax.YLim = img_ylim;
cax.ZLim = img_zlim;
cax.CLim = img_clim;

colormap(cax,cmap_isoluminant75);
cax.Color = [0.1 0.1 0.1];
cax.GridAlpha = 1;
cax.GridColor = [0.2 0.2 0.2];
axis(cax,'vis3d');
cb = colorbar(cax,'Location','southoutside');
cb.Label.String = 'height (nm)';
cax.DataAspectRatio = [1 1 0.1];



%%
ax_xlim = subplot(4,1,1,axes(limitphdl));
ax_ylim = subplot(4,1,2,axes(limitphdl));
ax_zlim = subplot(4,1,3,axes(limitphdl));
ax_clim = subplot(4,1,4,axes(limitphdl));

hx = histogram(ax_xlim,pixelSize_nm*TNT_locData.('x'));
ax_xlim.NextPlot = 'add';
ax_xlim.HandleVisibility = 'callback';
plot(ax_xlim,movmean(hx.BinEdges,2,'Endpoints','discard'),hx.Values,'LineStyle','none');
dualcursor(img_xlim,nan(2,2),'b',@(x,y)callback_passtrough('',@()callback_updateLimit(cax,'XLim',ax_xlim)),ax_xlim);

hy = histogram(ax_ylim,pixelSize_nm*TNT_locData.('y'));
ax_ylim.NextPlot = 'add';
ax_ylim.HandleVisibility = 'callback';
plot(ax_ylim,movmean(hy.BinEdges,2,'Endpoints','discard'),hy.Values,'LineStyle','none');
dualcursor(img_ylim,nan(2,2),'b',@(x,y)callback_passtrough('',@()callback_updateLimit(cax,'YLim',ax_ylim)),ax_ylim);

hz = histogram(ax_zlim,TNT_locData.('z'));
ax_zlim.NextPlot = 'add';
ax_zlim.HandleVisibility = 'callback';
plot(ax_zlim,movmean(hz.BinEdges,2,'Endpoints','discard'),hz.Values,'LineStyle','none');
dualcursor(img_zlim,nan(2,2),'b',@(x,y)callback_passtrough('',@()callback_updateLimit(cax,'ZLim',ax_zlim)),ax_zlim);

hc = histogram(ax_clim,TNT_locData.('z'));
ax_clim.NextPlot = 'add';
ax_clim.HandleVisibility = 'callback';
plot(ax_clim,movmean(hc.BinEdges,2,'Endpoints','discard'),hc.Values,'LineStyle','none');
dualcursor(img_clim,nan(2,2),'b',@(x,y)callback_passtrough('',@()callback_updateLimit(cax,'CLim',ax_clim)),ax_clim);
%%
function callback_export(~,~)
    if ~isgraphics(profile_loi)
        return
    end
    locInMask = updateMask(profile_loi,[]);
    
    outstruct = struct('position',profile_loi.Position/image_superResMag,'profile_axes',profile_hdl(1).XData,'profile_intensity',profile_hdl(1).YData,'profile_locData',locInMask);
    if ~isnan(profile_param)
        outstruct.(['profile_' profile_param]) = profile_hdl(2).YData;
    end
    assignin('base','TNT_profile_data',outstruct);
end
end

function out = callback_passtrough(out,callback)
    if isa(callback,'function_handle')
        callback();
    end
end

function callback_updateLimit(ax,limit,cursorax)
    %Update delta calculation
    cursors = findobj(cursorax,'Tag','Cursor');
    if numel(cursors)==2
        %Positions of two dualcursors
        xy1 = getappdata(cursors(1),'Coordinates');
        xy2 = getappdata(cursors(2),'Coordinates');
        
        newLim = [min(xy1(1),xy2(1)),max(xy1(1),xy2(1))];
        if newLim(1)==newLim(2)
            newLim = newLim+[-1,1]*eps(newLim(1));
        end
        set(ax,limit,newLim)
    end
end

% Callback for edit fields containing floats. Checks if a correct
% number was entered and restricts it to the given bounds.
function callback_FloatEdit(hObj,~, minVal, maxVal, flag, formatstring)
    if nargin<3 || isempty(minVal)
        minVal=-inf;
    end
    if nargin<4 || isempty(maxVal)
        maxVal=inf;
    end
    if nargin<6 || isempty(formatstring)
        formatstring='%.2f';
    end

    value = str2num(get(hObj, 'String')); %#ok<ST2NM> str2num distinguishes NaN and invalid input, str2double does not.
    if isempty(value)
        set(hObj,'ForegroundColor','r');
        set(hObj,'String','INVALID');
        uicontrol(hObj);
    elseif nargin>4 && isnan(value) && ischar(flag) && strcmpi(flag,'includeNaN')
        set(hObj,'ForegroundColor','k');
        set(hObj,'String','NaN');
    else
        value = max(minVal,value);
        value = min(maxVal,value);
        set(hObj,'ForegroundColor','k');
        set(hObj,'String',sprintf(formatstring,value));
    end
end