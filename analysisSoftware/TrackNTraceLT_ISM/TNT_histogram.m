function [fig] = TNT_histogram(TNT_locData,TNT_metaData)
if nargin==0
    try
        TNT_locData = evalin('base','TNT_locData');
        TNT_metaData = evalin('base','TNT_metaData');
    catch
        
    end
end
if ~exist('TNT_locData','var') || isempty(TNT_locData)
    warning('First input argument or workspace variable TNT_locData needs to be a table.');
    fig = gobjects(0);
    return
end

if ~istable(TNT_locData)
    if isnumeric(TNT_locData)
        if nargin>1 && iscellstr(TNT_metaData) && numel(TNT_metaData)==size(TNT_locData,2)
            varNames = TNT_metaData;
        else
            varNames = arrayfun(@(n)sprintf('col%g',n),1:size(TNT_locData,2),'UniformOutput',false);
        end
        TNT_locData = array2table(TNT_locData,'VariableNames',varNames);
    else
        warning('Unsupported data type: TNT_locData needs to be a table or matrix.');        
    end
end

%%
hist_min = 0.5;
hist_max = 99.5;
hist_percentile = true;
hist_avgTrack = true;
hist_nbins = 50;
hist_param = NaN;
hist_hdl = gobjects(1);
fit_type = 1;
fit_initValues = [];

data_track_ind = find(contains(TNT_locData.Properties.VariableNames,'track_ID','IgnoreCase',true),1);
%%
% pixelSize = TNT_metaData.pixelsize * 1e3;
fig = figure('Name','TNT Histogram fitter');
phdl = uipanel(fig,'Position',[0.01 0.01 0.98 0.9],'BackgroundColor',[0.98 0.98 0.98]);



popup_parameter = uicontrol('Style','popupmenu','String',[TNT_locData.Properties.VariableNames {'Overview'}],'Value',numel(TNT_locData.Properties.VariableNames)+1,...
                    'Units','normalized','Position',[0.01 0.93 0.18 0.05]);
prev_ui = popup_parameter;
check_avgTracks = uicontrol('Style','checkbox','String','avgTracks','Value',hist_avgTrack,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.13  prev_ui.Position(4)]);
if isempty(data_track_ind)
    hist_avgTrack = false;
    check_avgTracks.Value = hist_avgTrack;
    check_avgTracks.Enable = 'off';
end
prev_ui = check_avgTracks;
edit_min = uicontrol('Style','edit','String',sprintf('%g',hist_min),'Callback',{@callback_FloatEdit,[],[],[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.08  prev_ui.Position(4)]);              
prev_ui = edit_min;
edit_max = uicontrol('Style','edit','String',sprintf('%g',hist_max),'Callback',{@callback_FloatEdit,[],[],[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.08  prev_ui.Position(4)]);              
prev_ui = edit_max;
check_percentile = uicontrol('Style','checkbox','String','%','Value',hist_percentile,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.06  prev_ui.Position(4)]);
prev_ui = check_percentile;
edit_nbins = uicontrol('Style','edit','String',sprintf('%g',hist_nbins),'Callback',{@callback_FloatEdit,0,[],'includeNaN','%.0f'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = edit_nbins;
popup_fitmode = uicontrol('Style','popupmenu','String',fitHistogram(),'Value',fit_type,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.22  prev_ui.Position(4)]);
prev_ui = popup_fitmode;
button_init = uicontrol('Style','pushbutton','String','...','Callback',@callback_init,'Tooltip','Set inital values for fitting.',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.001 prev_ui.Position(2)  0.039  prev_ui.Position(4)]);
prev_ui = button_init;
button_fit = uicontrol('Style','pushbutton','String','Fit','Callback',@callback_fit,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.07  prev_ui.Position(4)]);

%%
callback_fit();

    function callback_updateData(TNT_locData_new,TNT_metaData_new)
        switch nargin
            case 0
                TNT_locData = evalin('base','TNT_locData');
                TNT_metaData = evalin('base','TNT_metaData');
            case 1
                TNT_locData = TNT_locData_new;
            case 2
                TNT_locData = TNT_locData_new;
                TNT_metaData = TNT_metaData_new;
        end
    end

    function callback_fit(~,~)
        hist_min = str2double(edit_min.String);
        hist_max = str2double(edit_max.String);
        hist_percentile = check_percentile.Value;
        hist_avgTrack = check_avgTracks.Value;
        hist_nbins = str2double(edit_nbins.String);
        hist_param = popup_parameter.Value;
        if fit_type ~= popup_fitmode.Value
            fit_type = popup_fitmode.Value;
            fit_initValues = [];
        end
        delete(phdl.Children);
        
        if hist_param>numel(TNT_locData.Properties.VariableNames)
            plotOverview();
        else
            hist_hdl = plotParameter(axes(phdl),hist_param);
            drawnow;
            [p,initValues,z,p_text,bincenter_fine,z_fine] = fitHistogram(fit_type,hist_hdl,[],fit_initValues);
            hist_hdl.Parent.NextPlot = 'add';
            
            plot(hist_hdl.Parent, bincenter_fine,z_fine,'LineWidth',2,'DisplayName','fit');
            if ~isempty(p_text)
                text(hist_hdl.Parent, 0.95, 0.99,p_text, 'Units','normalized','HorizontalAlignment','right','VerticalAlignment','top');
            end
            updateInitButtonColor();
        end
    end
    function callback_init(~,~)
        if popup_parameter.Value<=numel(TNT_locData.Properties.VariableNames)
            if hist_param ~= popup_parameter.Value
                % Plot the histogram if the selected parameter has changed
                temp_fitMode = popup_fitmode.Value;
                popup_fitmode.Value = 1;
                callback_fit();
                popup_fitmode.Value = temp_fitMode;
                fit_type = popup_fitmode.Value;
            end
            if fit_type ~= popup_fitmode.Value
                fit_type = popup_fitmode.Value;
                fit_initValues = [];
            end
            if isempty(fit_initValues)
                [~,fit_initValues,z,p_text,bincenter_fine,z_fine] = fitHistogram(fit_type,hist_hdl,[],fit_initValues);
            end
            [fitnames,fitmodels] = fitHistogram();
            if ~isempty(fitmodels(fit_type).FitInitParameter)
                uians = inputdlg(fitmodels(fit_type).FitInitParameter,[fitnames{fit_type}, ': Initial fit values'],[1 40],arrayfun(@(x)sprintf('%.5g',x),fit_initValues,'UniformOutput',false));
                if isempty(uians)
                    % Pressed cancel
                    fit_initValues = [];
                else
                    fit_initValues = cellfun(@str2double,uians);
                end
            end
        else
            fit_initValues = [];
        end
        updateInitButtonColor();
    end
    function updateInitButtonColor()
        DEFAULT_COLOR = [0.941,0.941,0.941]; % Default color of buttons.
        SELECTED_COLOR = [0.65, 0.9, 0]; % Color of selected button.
        if isempty(fit_initValues)
            button_init.BackgroundColor = DEFAULT_COLOR;
        else
            button_init.BackgroundColor = SELECTED_COLOR;
        end
    end
    function hist_obj = plotParameter(ax,pidx)
        if hist_avgTrack
            [~,~,idx] = unique(TNT_locData{:,data_track_ind});
            % faster than @mean argument
            pcount = accumarray(idx,1); 
            pdata = accumarray(idx,TNT_locData{:,pidx});
            pdata(pcount>0) = pdata./pcount;
        else
            pdata = TNT_locData{:,pidx};
        end
        if hist_percentile
            ind = pdata>=prctile(pdata,max(hist_min,0)) & pdata<=prctile(pdata,min(hist_max,100));
        else
            ind = pdata>=max(hist_min,-inf) & pdata<=min(hist_max,inf);
        end
        if hist_nbins>0
            hist_obj = histogram(ax,pdata(ind),'NumBins',hist_nbins);
        else
            hist_obj = histogram(ax,pdata(ind));            
        end
        title(TNT_locData.Properties.VariableNames{pidx},'Interpreter','none');
    end
    function plotOverview(~,~)
        thdl = tiledlayout(phdl,'flow');
        for pidx = 1:size(TNT_locData,2)
            plotParameter(nexttile(thdl),pidx);
        end
    end

%      function p0 = fitHistogram(h)
%         %%
%         fitTypes = {'No fit',...
%             '1x Gaussian','1x Gaussian + BG','2x Gaussian','2x Gaussian + BG','3x Gaussian','3x Gaussian + BG',...
%             '1x exponential','1x exponential + BG','2x exponential','2x exponential + BG','3x exponential','3x exponential + BG'};
%         if nargin==0
%             % return fit options
%             p0 = fitTypes;
%             return
%         else
%             p0 = [];
%             if numel(h.BinEdges)==2
%                 return
%             end
%         end
%         
%         bincenter = (h.BinEdges(1:end-1) + h.BinEdges(2:end))/2;
%         y = h.Values;
%         
%         
%         %%
%         
%         
% %         fitstruct = getFitFunction(fit_type,bincenter,y);
% %         f0 = inf;
% %         p0 = fitstruct.p0;
% % %         figure;
% %         for idx = 1:10
% %             [p,~,~,f] = Simplex_Handle(fitstruct.fitfun,p0,[],[],[],[],h.BinEdges(:),y(:),fitstruct.bgFlag);
% %             if f<f0
% %                 f0 = f;
% %                 p0 = p;
% %             end
% %         end
% %         h.Parent.NextPlot = 'add';
% %         tempax = linspace(h.BinEdges(1),h.BinEdges(end),500);
% %         [~,fitamp] = fitstruct.fitfun(p,h.BinEdges(:),y(:),fitstruct.bgFlag);
% %         [~,~,~,tempval] = fitstruct.fitfun(p0,tempax,fitamp*(numel(tempax)-1)/numel(bincenter),fitstruct.bgFlag);
% %         plot(h.Parent, movmean(tempax,2,'Endpoints','discard'),tempval,'LineWidth',2,'DisplayName','fit');
% %         if fitstruct.bgFlag
% %             fitText = [fitTypes{fit_type} ':' fitstruct.formatFit(p0,fitamp(1:end-1)) sprintf('\n+(%.4g)',fitamp(end))];
% %         else
% %             fitText = [fitTypes{fit_type} ':' fitstruct.formatFit(p0,fitamp)];
% %         end
% %         text(h.Parent, 0.95, 0.95,fitText, 'Units','normalized','HorizontalAlignment','right','VerticalAlignment','top');
%     end
end
% Callback for edit fields containing floats. Checks if a correct
% number was entered and restricts it to the given bounds.
    function callback_FloatEdit(hObj,~, minVal, maxVal, flag, formatstring)
        if nargin<3 || isempty(minVal)
            minVal=-inf;
        end
        if nargin<4 || isempty(maxVal)
            maxVal=inf;
        end
        if nargin<6 || isempty(formatstring)
            formatstring='%.2f';
        end
        
        value = str2num(get(hObj, 'String')); %#ok<ST2NM> str2num distinguishes NaN and invalid input, str2double does not.
        if isempty(value)
            set(hObj,'ForegroundColor','r');
            set(hObj,'String','INVALID');
            uicontrol(hObj);
        elseif nargin>4 && isnan(value) && ischar(flag) && strcmpi(flag,'includeNaN')
            set(hObj,'ForegroundColor','k');
            set(hObj,'String','NaN');
        else
            value = max(minVal,value);
            value = min(maxVal,value);
            set(hObj,'ForegroundColor','k');
            set(hObj,'String',sprintf(formatstring,value));
        end
    end
    
    function [err,c,zz,z] = nExpLSQ(p,edges,values,bgFlag)
        values = values(:);
        edges = edges(:);
        p = p(:)';
        if nargin<4 || isempty(bgFlag)
            bgFlag = false;
        end

        distcdf = @(x,tau)-tau.*exp(-x./tau); %erfc(x) = 1-erf(x), should be more precise. The constant offset gets cancled in the diffrence (next line);
        distfun = @(edges,tau)(distcdf(edges(2:end),tau(:)')-distcdf(edges(1:end-1),tau(:)'));

        zz = distfun(edges(:),p);
        if bgFlag
            zz = [zz, ones(size(zz,1),1)];
        end
        zz = zz./sum(zz,1);
        if numel(values)==size(zz,2)
            c = values;
            z = zz*c;
            err = NaN;
        else
            c = lsqnonneg(zz,values);
            z = zz*c;

%             err = sum(sum((values-z).^2./abs(z+eps))); % assuming histogram counts follow Poissonian distribution
%             err = sum((values-z).^2./abs(values)); 
            
            ind = values>0;
            err = 2*sum(z-values)-2*sum(values(ind).*log(z(ind)./values(ind))); % assuming histogram counts follow Poissonian distribution
%             plot(movmean(edges,2,'Endpoints','discard'),[values,z]);
%             drawnow;
        end
    end