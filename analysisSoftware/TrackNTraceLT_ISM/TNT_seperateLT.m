function lt = TNT_seperateLT(TNT_locData,TNT_metaData)
if nargin==0
    try
        TNT_locData = evalin('base','TNT_locData');
        TNT_metaData = evalin('base','TNT_metaData');
    catch
        
    end
end
if ~exist('TNT_locData','var') || isempty(TNT_locData)
    warning('First input argument or workspace variable TNT_locData needs to be a table.');
    return
end

if ~istable(TNT_locData)
    if isnumeric(TNT_locData)
        if nargin>1 && iscellstr(TNT_metaData) && numel(TNT_metaData)==size(TNT_locData,2)
            varNames = TNT_metaData;
        else
            varNames = arrayfun(@(n)sprintf('col%g',n),1:size(TNT_locData,2),'UniformOutput',false);
        end
        TNT_locData = array2table(TNT_locData,'VariableNames',varNames);
    else
        warning('Unsupported data type: TNT_locData needs to be a table or matrix.');        
    end
end

pixelSize = TNT_metaData.pixelsize * 1e3;
%% Postproc data -> struct
data = struct();
data.lt = accumarray(TNT_locData.Track_ID,TNT_locData.lt_tau,[],@mean);

%% fit lt histogramm whit gauss

data.lt = data.lt(data.lt>0);
[N,edges] = histcounts(data.lt,100);
f = fit(edges(1:end-1).',N.','gauss2');

%to de: b2 needs to be bigger than b1!!!
function diff = falsePositivDiff(sep)
    fPos1 = sqrt(pi)/2*f.a1.*(f.c1+f.c1*erf((f.b1-sep)/f.c1));
    fPos2 = sqrt(pi)/2*f.a2.*(f.c2-f.c2*erf((f.b2-sep)/f.c2));
    fPos1 = fPos1/arealt1;
    fPos2 = fPos2/arealt2;
    diff = abs(fPos2-fPos1);
end

minLT = min(data.lt);
maxLT = max(data.lt);

arealt1 = f.a1*sqrt(pi)*f.c1;
arealt2 = f.a2*sqrt(pi)*f.c2;

options = optimset('PlotFcns',@optimplotfval, 'TolX',1e-7);
lt = fminbnd(@falsePositivDiff,minLT,maxLT, options);

gauss1 = f.a1*exp(-((edges(1:end-1)-f.b1)/f.c1).^2);
gauss2 = f.a2*exp(-((edges(1:end-1)-f.b2)/f.c2).^2);

r = figure;
hold on
plot(edges(1:end-1).',N.','bo')
plot(edges(1:end-1).',gauss1.')
plot(edges(1:end-1).',gauss2.')
xline(lt,'Label','Lifetime with equal false positives','LabelOrientation','horizontal')
xlabel('lifetime [ns]')
ylabel('rel. frequency')
set(findall(r,'-property','FontSize'),'FontSize',17)
set(findall(r,'-property', 'MarkerSize'), 'MarkerSize', 12)
set(findall(r,'-property','LineWidth'),'LineWidth',1.5)

end




