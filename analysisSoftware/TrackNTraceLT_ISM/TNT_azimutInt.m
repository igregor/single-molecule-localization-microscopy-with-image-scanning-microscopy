function TNT_azimutInt(TNT_locData,TNT_metaData)
if nargin==0
    try
        TNT_locData = evalin('base','TNT_locData');
        TNT_metaData = evalin('base','TNT_metaData');
    catch
        
    end
end
if ~exist('TNT_locData','var') || isempty(TNT_locData)
    warning('First input argument or workspace variable TNT_locData needs to be a table.');
    fig = gobjects(0);
    return
end

if ~istable(TNT_locData)
    if isnumeric(TNT_locData)
        if nargin>1 && iscellstr(TNT_metaData) && numel(TNT_metaData)==size(TNT_locData,2)
            varNames = TNT_metaData;
        else
            varNames = arrayfun(@(n)sprintf('col%g',n),1:size(TNT_locData,2),'UniformOutput',false);
        end
        TNT_locData = array2table(TNT_locData,'VariableNames',varNames);
    else
        warning('Unsupported data type: TNT_locData needs to be a table or matrix.');        
    end
end

pixelSize = TNT_metaData.pixelsize * 1e3;
profile_sigma = 10; %nm
profile_hdl = gobjects(1);
profile_loi = gobjects(1);
profile_coi = gobjects(1);
profile_fit = gobjects(1);
profile_param = NaN;
profile_avgTrack = true;
profile_norm = true;

hist_hdl = gobjects(1);
hist_aratio = 5;

image_superResMag = 20;
image_reconstruction = [];
image_kern = [];
valid_loc = [];

data_track_ind = find(contains(TNT_locData.Properties.VariableNames,'track_ID','IgnoreCase',true),1);

%% UI

fig = figure('Name','TNT azimuthal Integration');
phdl = uipanel(fig,'Position',[0.01 0.01 0.98 0.9],'BackgroundColor',[0.98 0.98 0.98]);

popup_parameter = uicontrol('Style','popupmenu','String',[TNT_locData.Properties.VariableNames {'None'}],'Value',numel(TNT_locData.Properties.VariableNames)+1,...
                    'Units','normalized','Position',[0.01 0.935 0.16 0.045]);
prev_ui = popup_parameter;
check_avgTracks = uicontrol('Style','checkbox','String','avgTracks','Value',profile_avgTrack,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.13  prev_ui.Position(4)]);
if isempty(data_track_ind)
    profile_avgTrack = false;
    check_avgTracks.Value = profile_avgTrack;
    check_avgTracks.Enable = 'off';
end
prev_ui = check_avgTracks;
edit_sigma = uicontrol('Style','edit','String',sprintf('%g',profile_sigma),'Callback',{@callback_FloatEdit,[],[],[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = edit_sigma;
text_sigma = uicontrol('Style','text','String','sigma (nm)',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.1  prev_ui.Position(4)]);              
prev_ui = text_sigma;

button_update = uicontrol('Style','pushbutton','String','Apply','Callback',@callback_update,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.08  prev_ui.Position(4)]);
prev_ui = button_update; 
check_norm = uicontrol('Style','checkbox','String','norm','Value',profile_norm,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.08  prev_ui.Position(4)]);
prev_ui = check_norm;  

edit_ratio = uicontrol('Style','edit','String',sprintf('%g',hist_aratio),'Callback',{@callback_FloatEdit,[],[],[],'%g'},'Enable','off',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.04  prev_ui.Position(4)]);              
prev_ui = edit_ratio;
text_ratio = uicontrol('Style','text','String','ratio','Enable','off',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = text_ratio;
button_fit = uicontrol('Style','pushbutton','String','Fit','Enable','off',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);
prev_ui = button_fit; 

button_update = uicontrol('Style','pushbutton','String','Export to WS','Callback',@callback_export,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.15  prev_ui.Position(4)]);

%%

data = table();
warned = false;
callback_update([],[]);
%% Postproc data -> struct
% data = struct();
% data.lt = accumarray(TNT_locData.Track_ID,TNT_locData.lt_tau,[],@mean);
% data.np = accumarray(TNT_locData.Track_ID,TNT_locData.nphoton,[],@mean);
% data.x = accumarray(TNT_locData.Track_ID,TNT_locData.x,[],@mean);
% data.y = accumarray(TNT_locData.Track_ID,TNT_locData.y,[],@mean);
% data.chi2 = accumarray(TNT_locData.Track_ID,TNT_locData.lt_chi2,[],@mean);
% data.sigma = accumarray(TNT_locData.Track_ID,TNT_locData.sigma,[],@mean);
% data.frame = accumarray(TNT_locData.Track_ID,TNT_locData.Frame,[],@min);


function callback_update(~,~)
    %%
    if ~isgraphics(phdl)
        return;
    end
    %% Update parameter
    
    profile_sigma = str2double(edit_sigma.String);
    profile_avgTrack = check_avgTracks.Value;
    profile_param = popup_parameter.Value;
    if profile_param<1||profile_param>size(TNT_locData,2)
        profile_param = NaN;
    else
        profile_param = TNT_locData.Properties.VariableNames{profile_param};
    end
    
    if profile_avgTrack
        [~,~,idx] = unique(TNT_locData{:,data_track_ind});
        % faster than @mean argument
        pcount = accumarray(idx,1);
        for pidx = 1:size(TNT_locData,2)
            pname = TNT_locData.Properties.VariableNames{pidx};
            data.(pname) = accumarray(idx,TNT_locData.(pname));
            data.(pname)(pcount>0) = data.(pname)./pcount;
        end
    else
        data = TNT_locData;
    end
    
    
    %% Filter

    ind = data.nphoton>10;
    %
    valid_loc = 1/image_superResMag < data.x  ...
              & 1/image_superResMag < data.y  ...
              & ind;
          
    % Ronconstruct
    image_reconstruction = accumarray(round(image_superResMag.*[data.y(valid_loc), data.x(valid_loc)]),1);

    sigma = profile_sigma/pixelSize*image_superResMag;
    [jx,jy] = meshgrid(-2*sigma:2*sigma,-2*sigma:2*sigma);
    image_kern = exp(-(jx.^2+jy.^2)./2./sigma.^2);
    image_reconstruction = conv2(image_reconstruction,image_kern,'same');

    
    delete(phdl.Children);
    ax_img = subplot(1,2,1,axes(phdl));
    if isnan(profile_param)
        img = reconstructSMLM([data.x(ind),data.y(ind)],0.1,[],image_superResMag);
        imhdl = imagesc(ax_img,img);
        ax_img.CLim = [0 quantile(img(img>0),0.99)];
        ax_img.Colormap = cmap_heat();
        cb = colorbar(ax_img,'Location','southoutside');
        cb.Label.String = 'counts (a.u.)';
    else
        [img,imgLT] = reconstructSMLM([data.x(ind),data.y(ind),data.(profile_param)(ind)],0.1,[],image_superResMag);
        imhdl = imagesc(ax_img,imgLT);
        imhdl.AlphaData = img;
        imhdl.AlphaDataMapping = 'scaled';
        ax_img.ALim = [0 quantile(img(img>0),0.9)];
        ax_img.CLim = quantile(imgLT(img>0),[0.2 0.8])+[-eps eps];
        ax_img.Colormap = cmap_isoluminant75();
        ax_img.Color = [0 0 0];
        cb = colorbar(ax_img,'Location','southoutside');
        cb.Label.String = profile_param;
    end
    ax_img.DataAspectRatio = [1 1 1];
    ax_img.XRuler.Visible = 'off';
    ax_img.YRuler.Visible = 'off';
    ax_img.XLimMode = 'manual';
    ax_img.YLimMode = 'manual';
    TNTscalebar(ax_img,'Pixelsize',pixelSize/image_superResMag,'Unit',' nm')

    %% Draw roi
%     imagesc(ax_img,image_reconstruction);
%     colormap(ax_img,cmap_heat);
%     axis(ax_img,'image');
%     axis(ax_img,'off');
    % plot(colIdx,rowIdx,'o','MarkerFaceColor','w','MarkerEdgeColor','b');
%     caxis(ax_img,quantile(image_reconstruction(:),[0.80 0.999]));
    if isnan(profile_param)
        profile_loi = drawline(ax_img);
    else
        profile_loi = drawline(ax_img,'Color',[1 0 0]);
    end
    if ~isgraphics(profile_loi)
        return
    end
    profile_coi = images.roi.Circle(ax_img,'Deletable',false,'FaceSelectable',false,'InteractionsAllowed','none','FaceAlpha',0);
    profile_coi.Color = 1-0.5*(1-profile_coi.Color);
    profile_coi.LineWidth = profile_coi.LineWidth/2;
    
    profile_fit = images.roi.Circle(ax_img,'Deletable',false,'FaceSelectable',false,'InteractionsAllowed','none','FaceAlpha',0);
    profile_fit.Color = [1 1 1];
    profile_fit.LineWidth = profile_coi.LineWidth/2;
    profile_loi.bringToFront();
    
    hist_aratio = str2double(edit_ratio.String);
    if strcmpi(profile_param,'z') && hist_aratio>0
        ax_hist = subplot(2,2,4,axes(phdl));
        hist_hdl = imagesc(ax_hist,[]);
        ax_hist.XLabel.String = 'position (nm)';
        ax_hist.YLabel.String = 'height (nm)';
        colormap(ax_hist,cmap_heat);
        axis(ax_hist,'image');
        ax_hist.YDir = 'normal';
        
        set([edit_ratio text_ratio button_fit],'Enable','on');
        
        ax_profile = subplot(2,2,2,axes(phdl));
    else
        hist_hdl = gobjects(1);
        set([edit_ratio text_ratio button_fit],'Enable','off');
        
        ax_profile = subplot(1,2,2,axes(phdl));
    end
    yyaxis(ax_profile, 'left');
    profile_hdl = plot(ax_profile,[1 2],[1 2]);
    ax_profile.XLabel.String = 'position (nm)';
    ax_profile.YLabel.String = 'intensity';

    updateMask(profile_loi,[]);

    %%
    addlistener(profile_loi,'MovingROI',@updateMask);
    addlistener(profile_loi,'ROIMoved',@updateMask);
    check_norm.Callback = @(a,b)updateMask(profile_loi,[]);
    button_fit.Callback = @(a,b)callback_fit(profile_loi);
end
%%
function data_nm = updateMask(loi,~)
    if isempty(loi)
        loi = profile_loi;
    end
    circle_radius = sum(sqrt(sum(diff(loi.Position,1,1).^2,2))); % in px
    if circle_radius == 0
        return
    end
    circle_center = loi.Position(1,:);
    profile_coi.Center = circle_center;
    profile_coi.Radius = circle_radius;  
    profile_norm = check_norm.Value;  
    
    mask = createMask(profile_coi);
       
%     % Filter localisations in mask 
%     ind_inmask = valid_loc;
%     ind_inmask(valid_loc) = mask(sub2ind(size(mask),round(data.y(valid_loc)*image_superResMag),round(data.x(valid_loc)*image_superResMag)));
    data_nm = getLocInMask(mask,data,profile_param);

    if size(data_nm,1)>1
        
        calcRho = @(x,y,x0,y0)sqrt((x-x0).^2+(y-y0).^2);
        data_nm.rho = calcRho(data_nm.x,data_nm.y,circle_center(1)/image_superResMag*pixelSize,circle_center(2)/image_superResMag*pixelSize);
        rhoax = 0:1:(circle_radius*pixelSize/image_superResMag);
        if profile_norm
            [lint,lpos] = histcountswdist(data_nm.rho,1./data_nm.rho,profile_sigma,rhoax);
        else
            [lint,lpos] = histcountswdist(data_nm.rho,[],profile_sigma,rhoax);
        end
        set(profile_hdl(1),'XData',movmean(lpos,2,'Endpoints','discard'),'YData',lint);
        
        if ~isnan(profile_param)
            if numel(profile_hdl)<2
                yyaxis(profile_hdl.Parent, 'right');
                profile_hdl(2) = plot(profile_hdl.Parent,[1 2],[1 2]);
                profile_hdl(2).Parent.YAxis(2).Label.Interpreter = 'none';
            end
            ind = ~isnan(data_nm.(profile_param));
            if profile_norm
                [lt,lpos] = histcountswdist(data_nm.rho(ind),data_nm.(profile_param)(ind)./data_nm.rho(ind),profile_sigma,0:1:(circle_radius*pixelSize/image_superResMag));
                lt(lint>0) = lt(lint>0)./lint(lint>0);
                lt(lint.*movmean(rhoax,2,'Endpoints','discard')<1/sqrt(2*pi)/profile_sigma*exp(-2)) = NaN; % nan outside two sigma interval
            else
                [lt,lpos] = histcountswdist(data_nm.rho(ind),data_nm.(profile_param)(ind),profile_sigma,0:1:(circle_radius*pixelSize/image_superResMag));
                lt(lint>0) = lt(lint>0)./lint(lint>0);
                lt(lint<1/sqrt(2*pi)/profile_sigma*exp(-2)) = NaN; % nan outside two sigma interval
            end
            set(profile_hdl(2),'XData',movmean(lpos,2,'Endpoints','discard'),'YData',lt,'Visible','on');
            profile_hdl(2).Parent.YAxis(2).Label.String = profile_param;
            
            if isgraphics(hist_hdl)
                hist_aratio = str2double(edit_ratio.String);
                if hist_aratio>0
                    if profile_norm
                        imgData = accumarray(ceil(image_superResMag./pixelSize.*[hist_aratio*data_nm.(profile_param)(ind),data_nm.rho(ind)]),1./data_nm.rho(ind));
                    else
                        imgData = accumarray(round(image_superResMag./pixelSize.*[hist_aratio*data_nm.(profile_param)(ind),data_nm.rho(ind)]),1);
                    end
                    
                    sigma = profile_sigma/pixelSize*image_superResMag;
                    [jx,jy] = meshgrid(-2*sigma:2*sigma,-2*sigma:2*sigma);
                    image_kern = exp(-(jx.^2+jy.^2)./2./sigma.^2);
                    imgData = conv2(imgData,image_kern,'same');
                    hist_hdl.CData = imgData;
                    
                    hist_hdl.XData = rhoax([1 end]);
                    hist_hdl.YData = [0 1.05*max(data_nm.(profile_param)(ind))];
                    hist_hdl.Parent.XLim = hist_hdl.XData([1 end]);
                    hist_hdl.Parent.YLim = hist_hdl.YData([1 end]);
                    hist_hdl.Parent.DataAspectRatio(1:2) = [hist_aratio 1];
                    hist_hdl.Parent.CLim = [0 quantile(imgData(:),0.999)];
                else
                    hist_hdl.CData = [];
                end
            end
            
        elseif numel(profile_hdl)>1
            profile_hdl(2).Visible = 'off';
        end
    else
        set(profile_hdl(1),'XData',[0 circle_radius*pixelSize/image_superResMag],'YData',[0 0]);
        set(profile_hdl(end),'XData',[0 circle_radius*pixelSize/image_superResMag],'YData',[0 0]);
    end
    profile_hdl(1).Parent.XLim = [0 circle_radius*pixelSize/image_superResMag];
end

function data = getLocInMask(mask,data,param)
    % Filter localisations in mask
    % Convert x and y to nm
    ind_inmask = valid_loc;
    ind_inmask(valid_loc) = mask(sub2ind(size(mask),round(data.y(valid_loc)*image_superResMag),round(data.x(valid_loc)*image_superResMag)));

    if any(strcmpi(TNT_locData.Properties.VariableNames,param))
        data = data(ind_inmask,{'x','y',param});
    else
        data = data(ind_inmask,{'x','y'});
    end
    data.x = data.x * pixelSize;
    data.y = data.y * pixelSize;
end

function callback_fit(loi)
    data_nm = updateMask(loi,[]);
    
    ind = ~isnan(data_nm.x) & ~isnan(data_nm.y) & ~isnan(data_nm.z);
    initR0 = max(data_nm.rho(ind));
    
    initH = quantile(data_nm.z(ind),0.05);
    
    calcR = @(x,y,z,x0,y0,z0)sqrt((x-x0).^2+(y-y0).^2+(z-z0).^2);
    % p = [R0,X0,Y0,H];
    errfun = @(p,x,y,z,weight)sum((p(1)-calcR(x,y,z,p(2),p(3),p(1)+p(4))).^2.*weight);
    
    initX0 = trimmean(data_nm.x(ind),0.8);
    initY0 = trimmean(data_nm.y(ind),0.8);
    
    p0 = [initR0,initX0,initY0,initH];
    p = Simplex_Handle(errfun,p0,[0.5*initR0,initX0-200,initY0-200,0],[10*initR0,initX0+200,initY0+200,100],[],[],data_nm.x(ind),data_nm.y(ind),data_nm.z(ind),1);

    % Joerg's LSQ
%     loc_xyz_nm = [data_nm.x(ind),data_nm.y(ind),data_nm.z(ind)];
%     ssum = @(n)n.*(n-1)/2; % sum of all positive integers <= n
%     loc_n = size(loc_xyz_nm,1);
%     loc_M = nan(ssum(loc_n-1),3);
%     loc_v = nan(ssum(loc_n-1),1);
%     loc_idxp = 1;
%     for loc_idx = 1:loc_n
%         loc_ind = (loc_idx-1)*loc_n - ssum(loc_idx+1)'+2;
%         loc_M(loc_ind:loc_ind-1+loc_n-loc_idx,:) = loc_xyz_nm(loc_idx,:)-loc_xyz_nm(loc_idx+1:end,:);
%         loc_v(loc_ind:loc_ind-1+loc_n-loc_idx,:) = sum(loc_xyz_nm(loc_idx,:).^2,2)-sum(loc_xyz_nm(loc_idx+1:end,:).^2,2);
%     end
%     
%     % loc_centre = 1/2 * inv(transpose(loc_M)*loc_M)*(transpose(loc_M)*loc_v);
%     loc_centre = 1/2 * ((transpose(loc_M)*loc_M)\(transpose(loc_M)*loc_v));
%     
%     % Define the radius as average distance from the centre
%     loc_R = mean(sqrt(sum((loc_xyz_nm - loc_centre(:)').^2,2)));
%     p = [loc_R,loc_centre(1),loc_centre(2),loc_centre(3)-loc_R];
    

    delete(findobj(loi.Parent,'Tag','fit'));
    rectangle(loi.Parent,'Position',[p(2)-p(1),p(3)-p(1),p(1)*2,p(1)*2]/pixelSize*image_superResMag,'EdgeColor',[1 1 1],'Curvature',1,'Tag','fit')
    
    delete(findobj(hist_hdl.Parent,'Tag','fit'));
    line(hist_hdl.Parent,linspace(0,initR0),(p(1)-sqrt(p(1)^2-(linspace(0,initR0)).^2)+p(4)),'Color',[0.2769 0.6372 0.9686],'Tag','fit');
    text(hist_hdl.Parent,0.05,0.95,sprintf('r = %.0f nm\nh = %.0f nm',p(1),p(4)),'Units','normalized','HorizontalAlignment','left','VerticalAlignment','top','Color',[0.2769 0.6372 0.9686],'Tag','fit');
    
    
end

function callback_export(~,~)
    data_nm = updateMask(profile_loi,[]);
    
    warning('Not implemented yet.');
%     outstruct = struct('position',profile_loi.Position/image_superResMag,'profile_axes',profile_hdl(1).XData,'profile_intensity',profile_hdl(1).YData);
%     if ~isnan(profile_param)
%         outstruct.(['profile_' profile_param]) = profile_hdl(2).YData;
%     end
%     assignin('base','TNT_azInt_data',outstruct);
end
end

% Callback for edit fields containing floats. Checks if a correct
% number was entered and restricts it to the given bounds.
    function value = callback_FloatEdit(hObj,~, minVal, maxVal, flag, formatstring)
        if nargin<3 || isempty(minVal)
            minVal=-inf;
        end
        if nargin<4 || isempty(maxVal)
            maxVal=inf;
        end
        if nargin<6 || isempty(formatstring)
            formatstring='%.2f';
        end
        
        value = str2num(get(hObj, 'String')); %#ok<ST2NM> str2num distinguishes NaN and invalid input, str2double does not.
        if isempty(value)
            set(hObj,'ForegroundColor','r');
            set(hObj,'String','INVALID');
            uicontrol(hObj);
        elseif nargin>4 && isnan(value) && ischar(flag) && strcmpi(flag,'includeNaN')
            set(hObj,'ForegroundColor','k');
            set(hObj,'String','NaN');
        else
            value = max(minVal,value);
            value = min(maxVal,value);
            set(hObj,'ForegroundColor','k');
            set(hObj,'String',sprintf(formatstring,value));
        end
    end
    
    function [n,edges,bin] = histcountswdist(x, w, sigma, varargin)
    %HISTCOUNTSW Weighted Histogram Bin Counts
    % [n,edges,bin] = histcountsw(x, w, sigma, varargin)
    % Like histcounts but with an additional weighting of the data x
    % with w. For w = 1 they give identical output.
    % For sigma>0 a gaussian distribution with the width sigma is integrated
    % over the bins and summed up for all x.
    % Elements with the weight NaN are excluded.
    % See HISTCOUNTS for more details.
    %
    % There are the following additional normaizaltion options:
    %  'weights' (Default): Normalized to the weight of the counts.
    %  'cumweight'        : Cummulative weights.
    %  'weightdensity'    : Same as weights, but divded by the bin width.
    % For the options 'count', 'cumcount' and 'countdensity' the weights are
    % normalized to a mean of 1.
    %
    %
    %
    % Christoph Thiele, 2019

        if nargin<2 || isempty(w)
            w = 1;
        elseif ischar(w)
            if nargin>2
                varargin = [{w}, {sigma}, varargin];            
                sigma = [];
            else
                varargin = [{w}, varargin];
            end
            w = 1;
        end
        if nargin<3 || isempty(sigma)
            sigma = 0;
        elseif ischar(sigma)
            varargin = [{sigma}, varargin];
            sigma = 0;
        end

        ind = isnan(w) & isnan(sigma);
        if any(ind)
            w = w(~ind);
            x = x(~ind);
            sigma = sigma(~ind);
        end
    %     norm = {'count', 'probability', 'countdensity', 'pdf', 'cumcount', 'cdf'};
        normidx = find(strcmpi(varargin,'Normalization'),1,'last');
        if ~isempty(normidx) 
            if numel(normidx)+1>numel(varargin)
                error(message('MATLAB:histcounts:ArgNameValueMismatch'));
            end
            norm = varargin{normidx+1};

            if contains(norm, 'weight')
                varargin{normidx+1} = strrep(norm,'weight','count');
            elseif contains(norm, 'count')
                w = w/sum(w(:))*numel(w);
            end
        else
            norm ='weights';
        end

        [n,edges,bin] = histcounts(x, varargin{:});

        % expand w if singleton
        if isscalar(w)
            w = w * ones(size(x));
        end    
        % expand sigma if singleton
        if isscalar(sigma)
            sigma = sigma * ones(size(x));
        end

        if all(sigma==0) % No distribution
            ind = bin~=0; % zero indicates values out of the bounds or NaNs.
            if any(ind)
                bin = bin(:);
                w = w(:);
                n = accumarray(bin(ind),w(ind),size(n'))';% sum up w for each bin.            
            end % else keep zeros from histcounts 
        else % distribution for each value
    %         distcdf = @(x,mu,sigma)1/2*(1+erf((x-mu)./sqrt(2.*sigma.^2)));
            distcdf = @(x,mu,sigma)-0.5*erfc((x-mu)./sqrt(2.*sigma.^2)); %erfc(x) = 1-erf(x), should be more precise. The constant offset gets cancled in the diffrence (next line);
            distfun = @(edges,mu,sigma)distcdf(edges(2:end),mu,sigma)-distcdf(edges(1:end-1),mu,sigma);

            ind = ~isnan(x) & sigma>=0;
            if any(ind)
                sigma = max(sigma(:),eps);
                x = x(:);
                w = w(:);
                n = sum(w(ind).*distfun(edges(:)',x(ind),sigma(ind)),1);
            end % else keep zeros from histcounts 
        end
        switch norm
            case {'countdensity', 'weightdensity'}
                n = n./double(diff(edges));
            case {'cumcount', 'cumweight'}
                n = cumsum(n);
            case 'probability'
                n = n / sum(w);
            case 'pdf'
                n = n/sum(w)./double(diff(edges));
            case 'cdf'
                n = cumsum(n / sum(w));
        end
    end

