function TNT_crosssection(TNT_locData,TNT_metaData)
if nargin==0
    try
        TNT_locData = evalin('base','TNT_locData');
        TNT_metaData = evalin('base','TNT_metaData');
    catch
        
    end
end
if ~exist('TNT_locData','var') || isempty(TNT_locData)
    warning('First input argument or workspace variable TNT_locData needs to be a table.');
    fig = gobjects(0);
    return
end

if ~istable(TNT_locData)
    if isnumeric(TNT_locData)
        if nargin>1 && iscellstr(TNT_metaData) && numel(TNT_metaData)==size(TNT_locData,2)
            varNames = TNT_metaData;
        else
            varNames = arrayfun(@(n)sprintf('col%g',n),1:size(TNT_locData,2),'UniformOutput',false);
        end
        TNT_locData = array2table(TNT_locData,'VariableNames',varNames);
    else
        warning('Unsupported data type: TNT_locData needs to be a table or matrix.');        
    end
end

if isfield(TNT_metaData, 'pixelsize')
    pixelSize = TNT_metaData.pixelsize * 1e3;
else
    pixelSize = 100; % assume 100 nm pixel
    warning('TNT_metaData.pixelsize not set: assuming 100 nm pixels.');
end
profile_sigma = 10; %nm
profile_width = 200; %nm
profile_hdl = gobjects(1);
profile_loi = gobjects(1);
profile_patch = gobjects(1);
profile_fit = gobjects(1);
profile_param = NaN;
profile_avgTrack = true;

hist_hdl = gobjects(1);
hist_aratio = 5;

image_superResMag = 20;
image_reconstruction = [];
image_kern = [];
valid_loc = [];
param_binwidth0 = 1;

data_track_ind = find(contains(TNT_locData.Properties.VariableNames,'track_ID','IgnoreCase',true),1);

%% UI

fig = figure('Name','TNT crosssection');
phdl = uipanel(fig,'Position',[0.01 0.01 0.98 0.9],'BackgroundColor',[0.98 0.98 0.98]);

popup_parameter = uicontrol('Style','popupmenu','String',[TNT_locData.Properties.VariableNames {'None'}],'Value',numel(TNT_locData.Properties.VariableNames)+1,...
                    'Units','normalized','Position',[0.01 0.935 0.16 0.045]);
prev_ui = popup_parameter;
check_avgTracks = uicontrol('Style','checkbox','String','avgTracks','Value',profile_avgTrack,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.105  prev_ui.Position(4)]);
if isempty(data_track_ind)
    profile_avgTrack = false;
    check_avgTracks.Value = profile_avgTrack;
    check_avgTracks.Enable = 'off';
end
prev_ui = check_avgTracks;
edit_sigma = uicontrol('Style','edit','String',sprintf('%g',profile_sigma),'Callback',{@callback_FloatEdit,[],[],[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = edit_sigma;
text_sigma = uicontrol('Style','text','String','sigma (nm)',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.1  prev_ui.Position(4)]);              
prev_ui = text_sigma;
edit_width = uicontrol('Style','edit','String',sprintf('%g',profile_width),'Callback',{@callback_FloatEdit,[],[],[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.07  prev_ui.Position(4)]);              
prev_ui = edit_width;
text_width = uicontrol('Style','text','String','width (nm)',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.1  prev_ui.Position(4)]);              
prev_ui = text_width;

button_update = uicontrol('Style','pushbutton','String','Apply','Callback',@callback_update,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.08  prev_ui.Position(4)]);
prev_ui = button_update; 

edit_ratio = uicontrol('Style','edit','String',sprintf('%g',hist_aratio),'Callback',{@callback_FloatEdit,[],[],[],'%g'},'Enable','off',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.04  prev_ui.Position(4)]);              
prev_ui = edit_ratio;
text_ratio = uicontrol('Style','text','String','ratio','Enable','off',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.045  prev_ui.Position(4)]);              
prev_ui = text_ratio;

button_update = uicontrol('Style','pushbutton','String','Export to WS','Callback',@callback_export,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.15  prev_ui.Position(4)]);

%%

data = table();
warned = false;
callback_update([],[]);
%% Postproc data -> struct
% data = struct();
% data.lt = accumarray(TNT_locData.Track_ID,TNT_locData.lt_tau,[],@mean);
% data.np = accumarray(TNT_locData.Track_ID,TNT_locData.nphoton,[],@mean);
% data.x = accumarray(TNT_locData.Track_ID,TNT_locData.x,[],@mean);
% data.y = accumarray(TNT_locData.Track_ID,TNT_locData.y,[],@mean);
% data.chi2 = accumarray(TNT_locData.Track_ID,TNT_locData.lt_chi2,[],@mean);
% data.sigma = accumarray(TNT_locData.Track_ID,TNT_locData.sigma,[],@mean);
% data.frame = accumarray(TNT_locData.Track_ID,TNT_locData.Frame,[],@min);


function callback_update(~,~)
    %%
    if ~isgraphics(phdl)
        return;
    end
    %% Update parameter
    
    profile_sigma = str2double(edit_sigma.String);
    profile_width = str2double(edit_width.String);
    profile_avgTrack = check_avgTracks.Value;
    profile_param = popup_parameter.Value;
    if profile_param<1||profile_param>size(TNT_locData,2)
        profile_param = NaN;
    else
        profile_param = TNT_locData.Properties.VariableNames{profile_param};
    end
    
    if profile_avgTrack
        [~,~,idx] = unique(TNT_locData{:,data_track_ind});
        % faster than @mean argument
        pcount = accumarray(idx,1);
        for pidx = 1:size(TNT_locData,2)
            pname = TNT_locData.Properties.VariableNames{pidx};
            data.(pname) = accumarray(idx,TNT_locData.(pname));
            data.(pname)(pcount>0) = data.(pname)./pcount;
        end
    else
        data = TNT_locData;
    end
    
    
    %% Filter

    ind = data.nphoton>10;
    %
    valid_loc = 1/image_superResMag < data.x  ...
              & 1/image_superResMag < data.y  ...
              & ind;
          
%     % Ronconstruct
%     image_reconstruction = accumarray(round(image_superResMag.*[data.y(valid_loc), data.x(valid_loc)]),1);
% 
%     sigma = profile_sigma/pixelSize*image_superResMag;
%     [jx,jy] = meshgrid(-2*sigma:2*sigma,-2*sigma:2*sigma);
%     image_kern = exp(-(jx.^2+jy.^2)./2./sigma.^2);
%     image_reconstruction = conv2(image_reconstruction,image_kern,'same');

    
    delete(phdl.Children);
    ax_img = subplot(1,2,1,axes(phdl));
    if isnan(profile_param)
        img = reconstructSMLM([data.x(ind),data.y(ind)],profile_sigma/pixelSize,[],image_superResMag);
        imhdl = imagesc(ax_img,img);
        ax_img.CLim = [0 quantile(img(img>0),0.99)];
        ax_img.Colormap = cmap_heat();
        cb = colorbar(ax_img,'Location','southoutside');
        cb.Label.String = 'counts (a.u.)';
    else
        [img,imgLT] = reconstructSMLM([data.x(ind),data.y(ind),data.(profile_param)(ind)],profile_sigma/pixelSize,[],image_superResMag);
        imhdl = imagesc(ax_img,imgLT);
        imhdl.AlphaData = img;
        imhdl.AlphaDataMapping = 'scaled';
        ax_img.ALim = [0 quantile(img(img>0),0.9)];
        ax_img.CLim = quantile(imgLT(img>0),[0.2 0.8])+[-eps eps];
        ax_img.Colormap = cmap_isoluminant75();
        ax_img.Color = [0 0 0];
        cb = colorbar(ax_img,'Location','southoutside');
        cb.Label.Interpreter = 'none';
        cb.Label.String = profile_param;
    end
    ax_img.DataAspectRatio = [1 1 1];
    ax_img.XRuler.Visible = 'off';
    ax_img.YRuler.Visible = 'off';
    ax_img.XLimMode = 'manual';
    ax_img.YLimMode = 'manual';
    TNTscalebar(ax_img,'Pixelsize',pixelSize/image_superResMag,'Unit',' nm')

    image_reconstruction = img;
    %% Draw roi
%     imagesc(ax_img,image_reconstruction);
%     colormap(ax_img,cmap_heat);
%     axis(ax_img,'image');
%     axis(ax_img,'off');
    % plot(colIdx,rowIdx,'o','MarkerFaceColor','w','MarkerEdgeColor','b');
%     caxis(ax_img,quantile(image_reconstruction(:),[0.80 0.999]));
    if isnan(profile_param)
        profile_loi = drawline(ax_img);
    else
        profile_loi = drawline(ax_img,'Color',[1 0 0]);
    end
    if ~isgraphics(profile_loi)
        return
    end
    
    profile_patch = patch(ax_img,nan,nan,nan);
    profile_patch.PickableParts  = 'none';
    profile_patch.FaceAlpha  = 0.2;
    
    lineradius = ceil(profile_width/2/pixelSize*image_superResMag);% nm -> px
    
    hist_aratio = str2double(edit_ratio.String);
    if strcmpi(profile_param,'z') && hist_aratio>0
        ax_hist = subplot(2,2,4,axes(phdl));
        hist_hdl = imagesc(ax_hist,[]);
        ax_hist.XLabel.String = 'position (nm)';
        ax_hist.YLabel.String = 'height (nm)';
        colormap(ax_hist,cmap_heat);
        axis(ax_hist,'image');
        ax_hist.YDir = 'normal';
        
        set([edit_ratio text_ratio],'Enable','on');
        
        ax_profile = subplot(2,2,2,axes(phdl));
        yyaxis(ax_profile, 'left');
    elseif ~any(isnan(profile_param)) && hist_aratio>0
        
        ax_hist = subplot(2,2,4,axes(phdl));
        hist_hdl = imagesc(ax_hist,[]);
        ax_hist.DataAspectRatioMode = 'auto';
        ax_hist.XLabel.String = 'position (nm)';
        ax_hist.YLabel.Interpreter = 'none';
        ax_hist.YLabel.String = profile_param;
        colormap(ax_hist,cmap_heat);
        ax_hist.YDir = 'normal';
        
        % determine a base bin width based on all data
        % Freedman-Diaconis rule
        x = data.(profile_param)(~isnan(data.(profile_param)));
        param_binwidth0 = 2*iqr(x(:))*numel(x)^(-1/3);
        
        set([edit_ratio text_ratio],'Enable','on');
        
        ax_profile = subplot(2,2,2,axes(phdl));
        yyaxis(ax_profile, 'left');
    else
        hist_hdl = gobjects(1);
        set([edit_ratio text_ratio],'Enable','off');
        
        ax_profile = subplot(1,2,2,axes(phdl));
    end
    profile_hdl = plot(ax_profile,[1 2],[1 2]);
    ax_profile.XLabel.String = 'position (nm)';
    ax_profile.YLabel.String = 'intensity';

    updateMask(profile_loi,[]);

    %%
    addlistener(profile_loi,'MovingROI',@updateMask);
    addlistener(profile_loi,'ROIMoved',@updateMask);
end
%%
function data_inMask = updateMask(loi,~)
    if isempty(loi)
        loi = profile_loi;
    end
    
    lineradius = ceil(profile_width/2/pixelSize*image_superResMag);% nm -> px
    patch_xy = [];
    mask_ind_offset = 0;
    mask = zeros(size(image_reconstruction));
    
    for pidx = 2:size(loi.Position,1)
        dx=loi.Position(pidx,1)-loi.Position(pidx-1,1);
        dy=loi.Position(pidx,2)-loi.Position(pidx-1,2);
        mask(...
            sub2ind( size(mask),...
             round(linspace(loi.Position(pidx,2),loi.Position(pidx-1,2),ceil(1+sqrt(dx^2+dy^2)))),...
             round(linspace(loi.Position(pidx,1),loi.Position(pidx-1,1),ceil(1+sqrt(dx^2+dy^2))))...
            )) = mask_ind_offset + pixelSize/image_superResMag*linspace(eps,sqrt(dx^2+dy^2),ceil(1+sqrt(dx^2+dy^2)));  
        mask_ind_offset = pixelSize/image_superResMag*sqrt(dx^2+dy^2);
        
        kern2 = zeros(2*lineradius+1);
        dxp = lineradius/sqrt(1+dy^2/dx^2)*sign(dx);
        dyp = lineradius/sqrt(1+dx^2/dy^2)*sign(dy);
        kern2(...
            sub2ind( size(kern2),...
             (size(kern2,1)+1)/2 + round(linspace(+dxp,-dxp,ceil(1+2*sqrt(dxp^2+dyp^2)))),...
             (size(kern2,2)+1)/2 + round(linspace(-dyp,+dyp,ceil(1+2*sqrt(dxp^2+dyp^2))))...
            )) = 1;
        kern2 = imdilate(kern2,ones(2));% closes gaps at steps
        
        % genrate patch coordinates
        patch_xy(end+(1:4),:) = reshape(loi.Position((-1:0)+pidx,:)'+[dyp;-dxp].*shiftdim([-1 1],-1),2,[])';
    end
    patch_xy = [patch_xy((2:4:end)+[-1; 0],:); flip(patch_xy((4:4:end)+[-1; 0],:),1)];
    set(profile_patch,'XData', patch_xy(:,1),'YData', patch_xy(:,2),'FaceVertexCData',[1 1 1]);
       
    mask_ind = conv2(mask,kern2,'same');
    mask = conv2(mask>0,kern2,'same');
    mask_ind(mask>0) = mask_ind(mask>0)./mask(mask>0);
    mask = mask>0;
    
    
    % Filter localisations in mask 
    ind_inmask = valid_loc;
    ind_inmask(valid_loc) = mask(sub2ind(size(mask),round(data.y(valid_loc)*image_superResMag),round(data.x(valid_loc)*image_superResMag)));
%     ind_inmask = find(ind_inmask);

    data_inMask = data(ind_inmask,:);
    if ~isempty(data_inMask)
        ind_linpos = mask_ind(sub2ind(size(mask),round(data_inMask.y*image_superResMag),round(data_inMask.x*image_superResMag)));
        [lint,lpos] = histcountswdist(ind_linpos,[],profile_sigma,0:1:mask_ind_offset);
        set(profile_hdl(1),'XData',movmean(lpos,2,'Endpoints','discard'),'YData',lint);
        
        if ~isnan(profile_param)
            if numel(profile_hdl)<2
                yyaxis(profile_hdl.Parent, 'right');
                profile_hdl(2) = plot(profile_hdl.Parent,[1 2],[1 2]);
                profile_hdl(2).Parent.YAxis(2).Label.Interpreter = 'none';
            end
            ind = ~isnan(data_inMask.(profile_param));
            [lt,lpos] = histcountswdist(ind_linpos(ind),data_inMask.(profile_param)(ind),profile_sigma,0:1:mask_ind_offset);
            lt(lint>0) = lt(lint>0)./lint(lint>0);
            lt(lint<1/sqrt(2*pi)/profile_sigma*exp(-2)) = NaN; % nan outside two sigma interval
            set(profile_hdl(2),'XData',movmean(lpos,2,'Endpoints','discard'),'YData',lt,'Visible','on');
            profile_hdl(2).Parent.YAxis(2).Label.String = profile_param;
            
            if isgraphics(hist_hdl)
                hist_aratio = str2double(edit_ratio.String);
                if hist_aratio>0
                    if strcmpi(profile_param,'z')
                        imgData = accumarray(ceil([hist_aratio*data_inMask.(profile_param)(ind),ind_linpos(ind)]),1,ceil([hist_aratio*max(data_inMask.(profile_param)(ind)),mask_ind_offset]));
                        
                        hist_hdl.YData = [0 max(data_inMask.(profile_param)(ind))];
                        hist_hdl.Parent.DataAspectRatio(1:2) = [hist_aratio 1];
                    else
                        [~,pedge,pbin] = histcounts(data_inMask.(profile_param)(ind),'BinWidth',param_binwidth0/hist_aratio);
                        imgData = accumarray(ceil([pbin,ind_linpos(ind)]),1,[numel(pedge)-1,ceil(mask_ind_offset)]);
                        hist_hdl.YData = pedge([1 end]);
                    end
                    
                    sigma = profile_sigma;
                    [jx,jy] = meshgrid(-2*sigma:2*sigma,-2*sigma:2*sigma);
                    image_kern = exp(-(jx.^2+jy.^2)./2./sigma.^2);
                    imgData = conv2(imgData,image_kern,'same');
                    hist_hdl.CData = imgData;
                    
%                     hist_hdl.XData = rhoax([1 end]);
                    hist_hdl.Parent.XLim = [0 mask_ind_offset];
                    hist_hdl.Parent.YLim = hist_hdl.YData([1 end]);
                    if any(imgData(:)>0)
                        hist_hdl.Parent.CLim = [0 quantile(imgData(:),0.999)+eps];
                    end
                else
                    hist_hdl.CData = [];
                end
            end
            
        elseif numel(profile_hdl)>1
            profile_hdl(2).Visible = 'off';
        end
        data_inMask.profile_pos = ind_linpos;
    else
        set(profile_hdl(end),'XData',[0 mask_ind_offset],'YData',[nan nan]);
        set(profile_hdl(1),'XData',[0 mask_ind_offset],'YData',[0 0]);
        data_inMask.profile_pos(:) = 0;
    end
%     profile_hdl(1).Parent.XLim = [0 mask_ind_offset];
end

function data = getLocInMask(mask,data,param)
    % Filter localisations in mask
    % Convert x and y to nm
    ind_inmask = valid_loc;
    ind_inmask(valid_loc) = mask(sub2ind(size(mask),round(data.y(valid_loc)*image_superResMag),round(data.x(valid_loc)*image_superResMag)));

    if any(strcmpi(TNT_locData.Properties.VariableNames,param))
        data = data(ind_inmask,{'x','y',param});
    else
        data = data(ind_inmask,{'x','y'});
    end
    data.x = data.x * pixelSize;
    data.y = data.y * pixelSize;
end

function callback_export(~,~)
    if ~isgraphics(profile_loi)
        return
    end
    locInMask = updateMask(profile_loi,[]);
    
    outstruct = struct('position',profile_loi.Position/image_superResMag,'profile_axes',profile_hdl(1).XData,'profile_intensity',profile_hdl(1).YData,'profile_locData',locInMask);
    if ~isnan(profile_param)
        outstruct.(['profile_' profile_param]) = profile_hdl(2).YData;
    end
    assignin('base','TNT_profile_data',outstruct);
end
end

% Callback for edit fields containing floats. Checks if a correct
% number was entered and restricts it to the given bounds.
    function value = callback_FloatEdit(hObj,~, minVal, maxVal, flag, formatstring)
        if nargin<3 || isempty(minVal)
            minVal=-inf;
        end
        if nargin<4 || isempty(maxVal)
            maxVal=inf;
        end
        if nargin<6 || isempty(formatstring)
            formatstring='%.2f';
        end
        
        value = str2num(get(hObj, 'String')); %#ok<ST2NM> str2num distinguishes NaN and invalid input, str2double does not.
        if isempty(value)
            set(hObj,'ForegroundColor','r');
            set(hObj,'String','INVALID');
            uicontrol(hObj);
        elseif nargin>4 && isnan(value) && ischar(flag) && strcmpi(flag,'includeNaN')
            set(hObj,'ForegroundColor','k');
            set(hObj,'String','NaN');
        else
            value = max(minVal,value);
            value = min(maxVal,value);
            set(hObj,'ForegroundColor','k');
            set(hObj,'String',sprintf(formatstring,value));
        end
    end
    
    function [n,edges,bin] = histcountswdist(x, w, sigma, varargin)
    %HISTCOUNTSW Weighted Histogram Bin Counts
    % [n,edges,bin] = histcountsw(x, w, sigma, varargin)
    % Like histcounts but with an additional weighting of the data x
    % with w. For w = 1 they give identical output.
    % For sigma>0 a gaussian distribution with the width sigma is integrated
    % over the bins and summed up for all x.
    % Elements with the weight NaN are excluded.
    % See HISTCOUNTS for more details.
    %
    % There are the following additional normaizaltion options:
    %  'weights' (Default): Normalized to the weight of the counts.
    %  'cumweight'        : Cummulative weights.
    %  'weightdensity'    : Same as weights, but divded by the bin width.
    % For the options 'count', 'cumcount' and 'countdensity' the weights are
    % normalized to a mean of 1.
    %
    %
    %
    % Christoph Thiele, 2019

        if nargin<2 || isempty(w)
            w = 1;
        elseif ischar(w)
            if nargin>2
                varargin = [{w}, {sigma}, varargin];            
                sigma = [];
            else
                varargin = [{w}, varargin];
            end
            w = 1;
        end
        if nargin<3 || isempty(sigma)
            sigma = 0;
        elseif ischar(sigma)
            varargin = [{sigma}, varargin];
            sigma = 0;
        end

        ind = isnan(w) & isnan(sigma);
        if any(ind)
            w = w(~ind);
            x = x(~ind);
            sigma = sigma(~ind);
        end
    %     norm = {'count', 'probability', 'countdensity', 'pdf', 'cumcount', 'cdf'};
        normidx = find(strcmpi(varargin,'Normalization'),1,'last');
        if ~isempty(normidx) 
            if numel(normidx)+1>numel(varargin)
                error(message('MATLAB:histcounts:ArgNameValueMismatch'));
            end
            norm = varargin{normidx+1};

            if contains(norm, 'weight')
                varargin{normidx+1} = strrep(norm,'weight','count');
            elseif contains(norm, 'count')
                w = w/sum(w(:))*numel(w);
            end
        else
            norm ='weights';
        end

        [n,edges,bin] = histcounts(x, varargin{:});

        % expand w if singleton
        if isscalar(w)
            w = w * ones(size(x));
        end    
        % expand sigma if singleton
        if isscalar(sigma)
            sigma = sigma * ones(size(x));
        end

        if all(sigma==0) % No distribution
            ind = bin~=0; % zero indicates values out of the bounds or NaNs.
            if any(ind)
                bin = bin(:);
                w = w(:);
                n = accumarray(bin(ind),w(ind),size(n'))';% sum up w for each bin.            
            end % else keep zeros from histcounts 
        else % distribution for each value
    %         distcdf = @(x,mu,sigma)1/2*(1+erf((x-mu)./sqrt(2.*sigma.^2)));
            distcdf = @(x,mu,sigma)-0.5*erfc((x-mu)./sqrt(2.*sigma.^2)); %erfc(x) = 1-erf(x), should be more precise. The constant offset gets cancled in the diffrence (next line);
            distfun = @(edges,mu,sigma)distcdf(edges(2:end),mu,sigma)-distcdf(edges(1:end-1),mu,sigma);

            ind = ~isnan(x) & sigma>=0;
            if any(ind)
                sigma = max(sigma(:),eps);
                x = x(:);
                w = w(:);
                n = sum(w(ind).*distfun(edges(:)',x(ind),sigma(ind)),1);
            end % else keep zeros from histcounts 
        end
        switch norm
            case {'countdensity', 'weightdensity'}
                n = n./double(diff(edges));
            case {'cumcount', 'cumweight'}
                n = cumsum(n);
            case 'probability'
                n = n / sum(w);
            case 'pdf'
                n = n/sum(w)./double(diff(edges));
            case 'cdf'
                n = cumsum(n / sum(w));
        end
    end

