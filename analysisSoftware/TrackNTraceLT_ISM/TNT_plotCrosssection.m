pixelSize = TNT_metaData.pixelsize * 1e3;
figure;

if numel(unique(TNT_profile_data.profile_locData.z))>1
    z_values = TNT_profile_data.profile_locData.z;
    z_label = 'z (nm)';
else
    z_values = TNT_profile_data.profile_locData.lt_tau*100;
    z_label = 'lifetime (0.01 ns)';
end
%% 
clf;
% imagesc(reconstructSMLM([TNT_profile_data.profile_locData.profile_pos,LT2z(TNT_profile_data.profile_locData.lt_tau)],5,[],1));
imhdl = imagesc(reconstructSMLM([TNT_profile_data.profile_locData.profile_pos,z_values],5,TNT_profile_data.profile_locData.Amp_Peak_.*TNT_profile_data.profile_locData.sigma.^2,1));
axis image;
axis xy;
grid on;
axis image;
ylabel(z_label);
xlabel('line position (nm)');
% cax = gca;
% cax.GridColor = [0.5 0.5 0.5];
% cax.GridAlpha = 0.4;
% colormap(gca,cmap_heat);
% xticks(0:25:500);
% yticks(0:25:500);

ax = imhdl.Parent;

ax.Units = 'normalized';
ax.Position = [0.1 0.1 0.7 0.8];

% pos_old = ax.Position;
pos_old = plotboxpos(ax);

ax_side = axes(ax.Parent,'Units','normalized','Position',[sum(pos_old([1 3])) pos_old(2) 0.12 pos_old(4)],'YAxisLocation','right','NextPlot','replacechildren');%,'PlotBoxAspectRatio', [ax.PlotBoxAspectRatio([2 1])/sum(ax.PlotBoxAspectRatio(1:2)) 1],'PlotBoxAspectRatioMode','manual');
histogram(ax_side, z_values,'BinWidth',5, 'Orientation', 'horizontal');
ax_side.YLim = ax.YLim;
linkaxes([ax,ax_side] ,'y');

ax_top = axes(ax.Parent,'Units','normalized','Position',[pos_old(1) sum(pos_old([2 4])) pos_old(3) 0.12],'XAxisLocation','top','NextPlot','replacechildren');%,'PlotBoxAspectRatio', [ax.PlotBoxAspectRatio([2 1])/sum(ax.PlotBoxAspectRatio(1:2)) 1],'PlotBoxAspectRatioMode','manual');
histogram(ax_top, TNT_profile_data.profile_locData.profile_pos,'BinWidth',5);
ax_top.XLim = ax.XLim;
linkaxes([ax,ax_top] ,'x');

set(ax.Parent,'SizeChangedFcn',@(e,p){...
    set(ax_side,'Position',feval(@(pos_old)[sum(pos_old([1 3])) pos_old(2) 0.12 pos_old(4)],plotboxpos(ax))),...
    set(ax_top,'Position',feval(@(pos_old)[pos_old(1) sum(pos_old([2 4])) pos_old(3) 0.12],plotboxpos(ax)))...
    });
