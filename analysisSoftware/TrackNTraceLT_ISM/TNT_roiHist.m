function TNT_profile(TNT_locData,TNT_metaData)
if nargin==0
    try
        TNT_locData = evalin('base','TNT_locData');
        TNT_metaData = evalin('base','TNT_metaData');
    catch
        
    end
end
if ~exist('TNT_locData','var') || isempty(TNT_locData)
    warning('First input argument or workspace variable TNT_locData needs to be a table.');
    fig = gobjects(0);
    return
end

if ~istable(TNT_locData)
    if isnumeric(TNT_locData)
        if nargin>1 && iscellstr(TNT_metaData) && numel(TNT_metaData)==size(TNT_locData,2)
            varNames = TNT_metaData;
        else
            varNames = arrayfun(@(n)sprintf('col%g',n),1:size(TNT_locData,2),'UniformOutput',false);
        end
        TNT_locData = array2table(TNT_locData,'VariableNames',varNames);
    else
        warning('Unsupported data type: TNT_locData needs to be a table or matrix.');        
    end
end

%%
pixelSize = TNT_metaData.pixelsize * 1e3;
img_sigma = 10; %nm
img_avgTrack = true;
img_hdl = gobjects(1);
img_roiType = 1;
img_roihdl = gobjects(1);
img_superResMag = 20;
img_reconstruction = [];
img_kern = [];
valid_loc = [];

hist_nbins = 50;
hist_param = NaN;
hist_hdl = gobjects(1);

data_track_ind = find(contains(TNT_locData.Properties.VariableNames,'track_ID','IgnoreCase',true),1);

hist_param = find(contains(TNT_locData.Properties.VariableNames,'lt_tau','IgnoreCase',true),1);
if isempty(hist_param)
    hist_param = 1;
end

%% UI

fig = figure('Name','TNT ROI histogram');
phdl = uipanel(fig,'Position',[0.01 0.01 0.98 0.9],'BackgroundColor',[0.98 0.98 0.98]);

popup_roitype = uicontrol('Style','popupmenu','String',{'rectangle','polygon','circle','elipse'},'Value',img_roiType,...
                    'Units','normalized','Position',[0.01 0.935 0.13 0.045]);
prev_ui = popup_roitype;

button_draw = uicontrol('Style','pushbutton','String','draw','Callback',@callback_update,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.1  prev_ui.Position(4)]);
prev_ui = button_draw;  

popup_parameter = uicontrol('Style','popupmenu','String',[TNT_locData.Properties.VariableNames],'Value',hist_param,'Callback',@updateMask,...
                    'Units','normalized','Position',[sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.18  prev_ui.Position(4)]);
prev_ui = popup_parameter;
check_avgTracks = uicontrol('Style','checkbox','String','avgTracks','Value',img_avgTrack,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.13  prev_ui.Position(4)]);
if isempty(data_track_ind)
    img_avgTrack = false;
    check_avgTracks.Value = img_avgTrack;
    check_avgTracks.Enable = 'off';
end
prev_ui = check_avgTracks;

edit_nbins = uicontrol('Style','edit','String',sprintf('%g',hist_nbins),'Callback',{@callback_FloatEdit,0,[],'includeNaN','%.0f'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = edit_nbins;

% popup_fitmode = uicontrol('Style','popupmenu','String',fitHistogram(),'Value',fit_type,...
%                     'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.22  prev_ui.Position(4)]);
% prev_ui = popup_fitmode;
% button_update = uicontrol('Style','pushbutton','String','Apply','Callback',@callback_update,...
%                     'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.1  prev_ui.Position(4)]);
% prev_ui = button_update;         

button_toTNThist = uicontrol('Style','pushbutton','String','Transfer to fit','Callback',@callback_toTNThist,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.15  prev_ui.Position(4)]);
prev_ui = button_toTNThist;

button_export = uicontrol('Style','pushbutton','String','Export to WS','Callback',@callback_export,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.15  prev_ui.Position(4)]);

%%

data = struct();
warned = false;
callback_update([],[]);

function callback_update(~,~)
    %%
    if ~isgraphics(phdl)
        return;
    end
    %% Update parameter
    
    img_avgTrack = check_avgTracks.Value;
    hist_param = popup_parameter.Value;
    img_roiType = popup_roitype.Value;
      
    if img_avgTrack
        [~,~,idx] = unique(TNT_locData{:,data_track_ind});
        % faster than @mean argument
        pcount = accumarray(idx,1);
        data = struct();
        for pidx = 1:size(TNT_locData,2)
            pname = TNT_locData.Properties.VariableNames{pidx};
            data.(pname) = accumarray(idx,TNT_locData.(pname));
            data.(pname)(pcount>0) = data.(pname)./pcount;
        end
    else
        data = TNT_locData;
    end
    
    %% Filter

    ind = data.nphoton>10;

    %
    valid_loc = 1/img_superResMag < data.x  ...
              & 1/img_superResMag < data.y  ...
              & ind;
    img_reconstruction = accumarray(round(img_superResMag.*[data.y(valid_loc), data.x(valid_loc)]),1);

    sigma = img_sigma/pixelSize*img_superResMag;
    [jx,jy] = meshgrid(-2*sigma:2*sigma,-2*sigma:2*sigma);
    img_kern = exp(-(jx.^2+jy.^2)./2./sigma.^2);
    img_reconstruction = conv2(img_reconstruction,img_kern,'same');

    %% Make mask and filter localisations
    delete(phdl.Children);
    ax_img = subplot(1,2,1,axes(phdl));
    img_hdl = imagesc(ax_img,img_reconstruction);
    colormap(ax_img,cmap_heat);
    axis(ax_img,'image');
    axis(ax_img,'off');
    % plot(colIdx,rowIdx,'o','MarkerFaceColor','w','MarkerEdgeColor','b');
    caxis(ax_img,quantile(img_reconstruction(:),[0.80 0.999]));
    TNTscalebar(ax_img,'Pixelsize',pixelSize/img_superResMag,'Unit',' nm')


    ax_profile = subplot(1,2,2,axes(phdl));
    hist_hdl = histogram(ax_profile);

    updateMask(img_roihdl,[]);


end
%%
function locInMask = updateMask(roi,~)
    if nargin==0 || isempty(roi) || isprop(roi,'Type') && ~startsWith(roi.Type,'images.roi')
        roi = img_roihdl;
    end
    hist_param = TNT_locData.Properties.VariableNames{popup_parameter.Value};
    hist_nbins = str2double(edit_nbins.String);
    
    newRoi = ~isgraphics(roi);
    switch img_roiType
        case 1 % rectangle
            if ~newRoi && ~strcmpi(roi.Type,'images.roi.rectangle')
                newRoi = true;
                delete(roi);
            end
            if newRoi
                roi = drawrectangle(img_hdl.Parent);
            end
        case 2
            if ~newRoi && ~strcmpi(roi.Type,'images.roi.polygon')
                newRoi = true;
                delete(roi);
            end
            if newRoi
                roi = drawpolygon(img_hdl.Parent);
            end
        case 3
            if ~newRoi && ~strcmpi(roi.Type,'images.roi.circle')
                newRoi = true;
                delete(roi);
            end
            if newRoi
                roi = drawcircle(img_hdl.Parent);
            end
        case 4
            if ~newRoi && ~strcmpi(roi.Type,'images.roi.ellipse')
                newRoi = true;
                delete(roi);
            end
            if newRoi
                roi = drawellipse(img_hdl.Parent);
            end
    end
    if newRoi
        img_roihdl = roi;
        addlistener(img_roihdl,'MovingROI',@updateMask);
        addlistener(img_roihdl,'ROIMoved',@updateMask);
    end
    mask = createMask(roi);
       
    % Filter localisations in mask 
    ind_inmask = valid_loc;
    ind_inmask(valid_loc) = mask(sub2ind(size(mask),round(data.y(valid_loc)*img_superResMag),round(data.x(valid_loc)*img_superResMag)));
    ind_inmask = find(ind_inmask);

    if ~isempty(ind_inmask)
        ind = ~isnan(data.(hist_param)(ind_inmask));
        hist_hdl = histogram(hist_hdl.Parent,data.(hist_param)(ind_inmask(ind)),hist_nbins);
    else
        hist_hdl = histogram(hist_hdl.Parent);
    end
    hist_hdl.Parent.XLabel.Interpreter = 'none';
    hist_hdl.Parent.XLabel.String = hist_param;
    
    if nargout>0
        data_table = struct2table(data);
        if ~isempty(ind_inmask)
            locInMask = data_table(ind_inmask,:);
        else
            locInMask = data_table([],:);
        end
    end
end
function callback_export(~,~)
    if ~isgraphics(img_roihdl)
        return
    end
    locInMask = updateMask(img_roihdl,[]);
    
    outstruct = struct('position',img_roihdl.Position/img_superResMag,'roi_locData',locInMask);
    assignin('base','TNT_roi_data',outstruct);
end
function callback_toTNThist(~,~)
    if ~isgraphics(img_roihdl)
        return
    end
    locInMask = updateMask(img_roihdl,[]);
    
    TNT_histogram(locInMask,TNT_metaData);
end
end

% Callback for edit fields containing floats. Checks if a correct
% number was entered and restricts it to the given bounds.
function callback_FloatEdit(hObj,~, minVal, maxVal, flag, formatstring)
    if nargin<3 || isempty(minVal)
        minVal=-inf;
    end
    if nargin<4 || isempty(maxVal)
        maxVal=inf;
    end
    if nargin<6 || isempty(formatstring)
        formatstring='%.2f';
    end

    value = str2num(get(hObj, 'String')); %#ok<ST2NM> str2num distinguishes NaN and invalid input, str2double does not.
    if isempty(value)
        set(hObj,'ForegroundColor','r');
        set(hObj,'String','INVALID');
        uicontrol(hObj);
    elseif nargin>4 && isnan(value) && ischar(flag) && strcmpi(flag,'includeNaN')
        set(hObj,'ForegroundColor','k');
        set(hObj,'String','NaN');
    else
        value = max(minVal,value);
        value = min(maxVal,value);
        set(hObj,'ForegroundColor','k');
        set(hObj,'String',sprintf(formatstring,value));
    end
end
