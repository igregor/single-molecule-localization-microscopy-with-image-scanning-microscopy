function [fig] = TNT_locPrec(TNT_locData,TNT_metaData)
if nargin==0
    try
        TNT_locData = evalin('base','TNT_locData');
        TNT_metaData = evalin('base','TNT_metaData');
    catch
        
    end
end
if ~exist('TNT_locData','var') || isempty(TNT_locData)
    warning('First input argument or workspace variable TNT_locData needs to be a table.');
    fig = gobjects(0);
    return
end

if ~istable(TNT_locData)
    if isnumeric(TNT_locData)
        if nargin>1 && iscellstr(TNT_metaData) && numel(TNT_metaData)==size(TNT_locData,2)
            varNames = TNT_metaData;
        else
            varNames = arrayfun(@(n)sprintf('col%g',n),1:size(TNT_locData,2),'UniformOutput',false);
        end
        TNT_locData = array2table(TNT_locData,'VariableNames',varNames);
    else
        warning('Unsupported data type: TNT_locData needs to be a table or matrix.');        
    end
end

pixelSize = TNT_metaData.pixelsize * 1e3;
%% Postproc data -> struct
data = struct();
data.np = accumarray(TNT_locData.Track_ID,TNT_locData.nphoton,[],@mean);
data.sigma = accumarray(TNT_locData.Track_ID,TNT_locData.sigma,[],@mean);
data.amp = accumarray(TNT_locData.Track_ID,TNT_locData.Amp_Peak_,[],@sum);
data.bg = accumarray(TNT_locData.Track_ID,TNT_locData.Background,[],@sum);
%% Calculate precision (Rieger)
% Rieger calls this uncertainty
gain = 1; % For correct photon conversion

N = data.amp .* 2*pi .* data.sigma.^2; %total number of photons
%merkwürdig
ind = N > 0;
N = N(ind);
data.bg = data.bg(ind);
data.sigma = data.sigma(ind);
tau = 2*pi*data.bg .* (data.sigma.^2+1/12)./N;
locPrecision = sqrt((gain*data.sigma.^2+1/12)./N.*(1+4*tau+sqrt(2*tau./(1+4*tau)))); % eq.7 in Rieger et al, DOI 10.1002/cphc.201300711

data.precisionRieger = pixelSize*locPrecision; % in nm
savePre = data.precisionRieger;
saveSig = data.sigma;
uisave({'savePre', 'saveSig'}, 'loc_per')
h = histogram(axes(figure),data.precisionRieger,'BinLimits',[0 50]);
histdata = h.Data;
histdata = histdata(~isnan(histdata));
ind = histdata< 50 & histdata >=0;
histdata = histdata(ind);
title('Rieger et al. (modified Mortensen)','FontWeight','normal');
text(0.95,0.95,sprintf('Mean: %.1f\nMedian: %.1f\nStd: %.1f\n',mean(histdata),median(histdata),std(histdata)),'HorizontalAlignment','right','VerticalAlignment','top','Units','normalized');
xlabel('loc. precision (nm)');
ylabel('frequency');


end