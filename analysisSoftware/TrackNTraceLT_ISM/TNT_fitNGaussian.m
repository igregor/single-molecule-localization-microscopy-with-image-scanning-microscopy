function TNT_fitNGaussian(n)
    if nargin<1
        n = 1;
    end
    %%
    
    h = findobj(gca,'Type','Histogram');
    
    if isempty(h)
        return
    end
    
    
    bincenter = (h.BinEdges(1:end-1) + h.BinEdges(2:end))/2;
    y = h.Values;
    
    hsum = sum(h.Values);
    hmean = sum(h.Values.*bincenter)./hsum;
    hstd = sqrt(abs(sum(h.Values.*bincenter.^2)-(sum(h.Values.*bincenter)).^2/hsum)/hsum)+eps; % +eps ensures sigma>0

    
%     nGaussianLSQ([hmean hstd],h.BinEdges,y);
    fit_gaussian_full_init = repmat([hmean hstd/n],1,n);
    fit_gaussian_full_init(1:2:end) = fit_gaussian_full_init(1:2:end)+hstd*4*((-(n-1)/2:(n-1)/2)/n);
    p0 = fit_gaussian_full_init;
    f0 = inf;
    for idx = 1:10
        [p,~,~,f] = Simplex_Handle(@nGaussianLSQ,p0,[],[],[],[],h.BinEdges(:),y(:));
        if f<f0
            f0 = f;
            p0 = p;
        end
    end
    hold on;
    tempax = linspace(h.BinEdges(1),h.BinEdges(end),500);
    [~,tempamp] = nGaussianLSQ(p,h.BinEdges(:),y(:));
    [~,~,~,tempval] = nGaussianLSQ(p0,tempax,tempamp);
    plot(h.Parent, movmean(tempax,2,'Endpoints','discard'),tempval,'LineWidth',2,'DisplayName','Gaussian fit');
    hold off;
    
%     
%     distcdf = @(x,mu,sigma)-0.5*erfc((x-mu)./sqrt(2.*sigma.^2)); %erfc(x) = 1-erf(x), should be more precise. The constant offset gets cancled in the diffrence (next line);
%     distfun = @(edges,mu,sigma,amp)(distcdf(edges(2:end),mu(:)',sigma(:)')-distcdf(edges(1:end-1),mu(:)',sigma(:)'))./(edges(2:end)-edges(1:end-1))*amp(:);
%     
%     %     errfun = @(p,edges,y) sum(sum(-y.*log(distfun(edges(:),p(1:3:end),p(2:3:end),p(3:3:end))+1e-30)+distfun(edges(:),p(1:3:end),p(2:3:end),p(3:3:end))));
%     %     errfun = @(p,edges,y) sum(sum((y-distfun(edges(:),p(1:3:end),p(2:3:end),p(3:3:end))).^2./distfun(edges(:),p(1:3:end),p(2:3:end),p(3:3:end))))/numel(edges);
%     %     errfun = @(p,edges,y) sum(sum((y-distfun(edges(:),p(1:3:end),p(2:3:end),p(3:3:end))).^2./y))/numel(edges);
%     
%     fit_gaussian_full_init = repmat([hmean hstd*1.5/n hsum/n*mean(diff(bincenter))],1,n);
%     fit_gaussian_full_init(1:3:end) = fit_gaussian_full_init(1:3:end)+hstd*2*((-(n-1)/2:(n-1)/2)/n);
%     
%     p = Simplex(errfun,fit_gaussian_full_init,0.*fit_gaussian_full_init,[],[],[],h.BinEdges(:),y(:));
%     %     p = Simplex(errfun,p,[1 1 0 1 1 0 1 1 0]'.*p,[1 1 100 1 1 100 1 1 100]'.*p,[],[],h.BinEdges(:),y(:));
%     %     p = Simplex(errfun,p,0.*p,[],[],[],h.BinEdges(:),y(:));
%     %     p = Simplex(errfun,p,[1 1 0 1 1 0 1 1 0]'.*p,[1 1 100 1 1 100 1 1 100]'.*p,[],[],h.BinEdges(:),y(:));
%     
%     hold on;
%     tempax = linspace(h.BinEdges(1),h.BinEdges(end),500);
%     plot(h.Parent, movmean(tempax,2,'Endpoints','discard'),distfun(tempax(:),p(1:3:end),p(2:3:end),p(3:3:end)),'LineWidth',2,'DisplayName','Gaussian fit');
%     hold off;
    
end

function [err,c,zz,z] = nGaussianLSQ(p,edges,values)
    values = values(:);
    edges = edges(:);
    p = p(:)';
    
    distcdf = @(x,mu,sigma)-0.5*erfc((x-mu)./sqrt(2.*sigma.^2)); %erfc(x) = 1-erf(x), should be more precise. The constant offset gets cancled in the diffrence (next line);
    distfun = @(edges,mu,sigma,amp)(distcdf(edges(2:end),mu(:)',sigma(:)')-distcdf(edges(1:end-1),mu(:)',sigma(:)'))./(edges(2:end)-edges(1:end-1)).*amp(:)';
    
    zz = distfun(edges(:),p(1:2:end),p(2:2:end),ones(size(p(1:2:end))));
    
    if numel(values)==numel(p)/2
        c = values;
        z = zz*c;
        err = NaN;
    else
        c = lsqnonneg(zz,values);
        z = zz*c;
        
        err = sum(sum((values-z).^2./abs(z))); % assuming histogram counts follow Poissonian distribution
    end
end