function options = TNT_getIRFparam(options,file)

%% UI

fig = figure('Name','TNT IRF fit');
phdl = uipanel(fig,'Position',[0.01 0.01 0.98 0.9],'BackgroundColor',[0.98 0.98 0.98],'HandleVisibility','callback');
ax = gobjects(1);


edit_file = uicontrol('Style','edit','String','file name','Enable','off',...
                    'Units','normalized','Position', [0.01 0.935 0.235 0.045]);              
prev_ui = edit_file;
button_selectFile = uicontrol('Style','pushbutton','String','Select','Callback',@callback_selectFile,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3])) prev_ui.Position(2)  0.07  prev_ui.Position(4)]);
prev_ui = button_selectFile;

text_alpha = uicontrol('Style','text','String','alpha',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = text_alpha;
edit_alpha = uicontrol('Style','edit','String','10','Callback',{@callback_fitParamChange,@callback_FloatEdit,1e-2,1e2,[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = edit_alpha;
text_beta = uicontrol('Style','text','String','beta',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = text_beta;

edit_beta = uicontrol('Style','edit','String','10','Callback',{@callback_fitParamChange,@callback_FloatEdit,1e-2,1e2,[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = edit_beta;

text_t0 = uicontrol('Style','text','String','t0',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.03  prev_ui.Position(4)]);              
prev_ui = text_t0;
edit_t0 = uicontrol('Style','edit','String','0.5','Callback',{@callback_fitParamChange,@callback_FloatEdit,0,inf,[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = edit_t0;

check_tau = uicontrol('Style','checkbox','String','tau','Value',false,'Callback',@callback_fitParamChange,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.07  prev_ui.Position(4)]);
prev_ui = check_tau;
edit_tau = uicontrol('Style','edit','String','1.8','Callback',{@callback_fitParamChange,@callback_FloatEdit,0,inf,[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = edit_tau;

text_b = uicontrol('Style','text','String','b',...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.02  prev_ui.Position(4)]);              
prev_ui = text_b;
edit_b = uicontrol('Style','edit','String','0.05','Callback',{@callback_fitParamChange,@callback_FloatEdit,0,1,[],'%g'},...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.005 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);              
prev_ui = edit_b;

button_update = uicontrol('Style','pushbutton','String','Fit','Callback',@callback_fit,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.01 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);             
prev_ui = button_update;
                
button_close = uicontrol('Style','pushbutton','String','Ok','Callback',@callback_export,...
                    'Units','normalized','Position', [sum(prev_ui.Position([1 3]))+0.02 prev_ui.Position(2)  0.05  prev_ui.Position(4)]);

%%

t = linspace(0,25,1e3);
tcspc = 1;
irfparam = [];
irfpdf = [];
irfhdl = gobjects(1);

import_plugins = [];
selected_import_plugin = nan;
accepted_formats = {};

if exist('file','var') && exist(file,'file')
    currentfile = file;
    fpath = fileparts(file);        
    loadFile(currentFile);
    plotTCSPC();
else
    currentFile = '';
    fpath = '';
end
%%
callbackFlag = nargin>0 && isstruct(options) && isfield(options,'IRF_alpha');
acceptChanges = [];
if callbackFlag
    
    callback_FloatEdit(edit_alpha,[],options.IRF_alpha,options.IRF_alpha,[],'%g');
    callback_FloatEdit(edit_beta, [],options.IRF_beta,options.IRF_beta,[],'%g');
    callback_FloatEdit(edit_t0,   [],options.IRF_t0,options.IRF_t0,[],'%g');
    
    callback_fitParamChange();% also plots IRF
    fig.WindowStyle = 'modal';
    fig.NumberTitle = 'off';
    uiwait(fig);
    if isempty(acceptChanges)
        if all(abs([options.IRF_alpha, options.IRF_beta, options.IRF_t0]-irfparam(1:3))<1e-2)
            acceptChanges = false;
        else
            answer = questdlg('Transfer new IRF parameters to TNT settings?','Save parameters', ...
                              'Yes','No','Yes');
            acceptChanges = strcmpi(answer,'yes');
        end
    end
    if acceptChanges && ~isempty(irfparam) && ~any(isnan(irfparam(1:3)))
        options.IRF_alpha = irfparam(1);
        options.IRF_beta = irfparam(2);
        options.IRF_t0 = irfparam(3);
    end
else    
    callback_fitParamChange();% also plots IRF
    if nargout>0
        uiwait(fig);
        options = irfparam;
    end
end
%% Callbacks
    function callback_selectFile(~,~)
        if isempty(accepted_formats)
            getPluginsAndFormats();
            if isempty(accepted_formats)
                return;
            end
        end
        if exist(fpath,'dir')
            [filename, fpath] = uigetfile(accepted_formats,'Select file to determine IRF',[fpath,filesep]);
        else
            [filename, fpath] = uigetfile(accepted_formats,'Select file to determine IRF');
        end
        if isfloat(filename)
            return
        end
        currentFile = [fpath filename];
        edit_file.String = filename;
        edit_file.Tooltip = currentFile;
        button_selectFile.Tooltip = currentFile;
        
        loadFile(currentFile);
        plotTCSPC();
    end
    function callback_fitParamChange(obj,evt,callback,varargin)
        if nargin>2 && isa(callback,'function_handle')
            callback(obj,evt,varargin{:});
        end
        if check_tau.Value ~= strcmpi(edit_tau.Enable,'on')
            if check_tau.Value
                edit_tau.Enable = 'on';
            else
                edit_tau.Enable = 'off';
            end
        end
        plotIRF();
    end
    function getPluginsAndFormats()
        [import_plugins] = loadImportPlugins();
        for iPlug = 1:numel(import_plugins)
            if import_plugins(iPlug).info.hasTCSPC
                accepted_formats = [accepted_formats; import_plugins(iPlug).info.supportedFormats]; %#ok<AGROW>
            end
        end
        accepted_formats = [{strjoin(accepted_formats(:,1),';'), 'Compatible formats'};accepted_formats];
    end
    function loadFile(file)
        if isempty(import_plugins)
            getPluginsAndFormats();
        end
        selected_import_plugin = selectImportPlugin(file,import_plugins);
        assert(selected_import_plugin>0,'No import plugin for file %s available.',file);
        ip = import_plugins(selected_import_plugin);
        assert(isfield(ip.info,'getTCSPC')&&isa(ip.info.getTCSPC,'function_handle'),'Import plugin cannot read TCSPC.');
        
        [tcspcdata,resolution] = ip.info.getTCSPC(file,inf);
        tcspc = accumarray(tcspcdata-min(tcspcdata)+1,1);
        t = (1:(numel(tcspc)))*resolution*1e9; % in ns
    end
    function plotTCSPC()
        delete(phdl.Children);
        ax = axes(phdl,'HandleVisibility','callback');
        semilogy(ax,t,tcspc,'b.');
        
        ax.XLim = [0 t(end)];
        ax(1).YLabel.String = 'counts';
        plotIRF();
    end
    function plotIRF()
        irfparam(1) = str2double(edit_alpha.String);
        irfparam(2) = str2double(edit_beta.String);
        irfparam(3) = str2double(edit_t0.String);
        if check_tau.Value
            irfparam(4) = str2double(edit_tau.String);
        else
            irfparam(4) = 0;
        end
        irfparam(5) = str2double(edit_b.String);
        
        if ~isempty(irfparam) && all(~isnan(irfparam))
            irfpdf = monoexpConvGamma(irfparam,t);
            % plot fit
            if ~isgraphics(irfhdl)
                if ~all(isgraphics(ax))
                    ax = axes(phdl,'HandleVisibility','callback');
                else                
                    ax(1).NextPlot = 'add';
                end
                irfhdl(1) = semilogy(ax(1),t,irfpdf*sum(tcspc),'r','LineWidth',2);
            else
                irfhdl(1).YData = irfpdf*sum(tcspc);
            end
            % plot irf if fit has lifetime component
            if check_tau.Value 
                irfWOtau = monoexpConvGamma(irfparam(:).*[1;1;1;0;1],t);
            else
                irfWOtau = nan(size(irfhdl(1).YData));
            end
            if numel(irfhdl)>1 && isgraphics(irfhdl(2))
                irfhdl(2).YData = irfWOtau/max(irfWOtau)*max(irfhdl(1).YData);
            else
                ax(1).NextPlot = 'add';
                irfhdl(2) = semilogy(ax(1),t,irfWOtau/max(irfWOtau)*max(irfhdl(1).YData),'k','LineWidth',2);
                irfhdl(2).Color = [0 0 0 0.3];
            end
            % plot residues
            if numel(tcspc)>1
                if numel(ax)<2 || ~isgraphics(ax(2))
                    subplot(3,1,1:2,ax(1));
                    ax(2) = subplot(3,1,3,axes(ax(1).Parent));
                end
                res = (tcspc(:)-irfhdl(1).YData(:))./sqrt(irfhdl(1).YData(:));
                plot(ax(2),t,res,'b.');
                yline(ax(2),0,'r','LineWidth',2);
                ax(2).XLim = ax(1).XLim;
                linkaxes(ax(1:2),'x');
                ax(2).XLabel.String = 'time (ns)';
                
                chi2 = sum((irfhdl(1).YData(:)-tcspc(:)).^2./abs(irfhdl(1).YData(:)))/(numel(t)-numel(irfparam)-1);
                
                fwhm_fun = @(pdf,t) t(find(pdf>max(pdf)/2,1,'last'))-t(find(pdf>max(pdf)/2,1));
                if check_tau.Value 
                    print_text = sprintf('red. \\chi^2: %.2f\n FWHM: %.2f',chi2,fwhm_fun(irfWOtau-min(irfWOtau),t));
                else
                    print_text = sprintf('red. \\chi^2: %.2f\n FWHM: %.2f',chi2,fwhm_fun(irfpdf-min(irfpdf),t));
                end
                if numel(ax)>2 && isgraphics(ax(3))
                    ax(3).String = print_text;
                else
                    ax(3) = text(ax(1),0.95,0.95,print_text,'HorizontalAlignment','right','VerticalAlignment','top','Units','normalized');
                end
            end
        end
    end

function callback_fit(~,~)
    %%
    if numel(tcspc)>1
        if check_tau.Value
            % determine lifetime with tailfit
            cutoff = 0.5;
            
            minLT = 0.1;
            maxLT = 10;
            
            tail_ind = find(t(tcspc==max(tcspc))+cutoff<t);
            
            n_lt = 500; % number of possible lifetime values
            n_bg = 50;  % number of possible background values
            
            taus_pm = linspace(max(minLT,eps),min(maxLT,t(end)),n_lt);
            bgs_pm = linspace(0,1-1/n_bg,n_bg-1).^2; % square linspace to sample low bg values more densely
            
            [taus_pm,bgs_pm] = meshgrid(taus_pm(:),bgs_pm(:)');
            taus_pm = taus_pm(:);
            bgs_pm = bgs_pm(:);
            
            t_delta = mean(diff(t));
            pfun_monoexp = @(tau,T,t) 1./tau.*exp(-t./tau)./(1-exp(-T./tau)) * t_delta; % Normalised monoexponetial decay
            pfun_monoexpBG = @(tau,b)b./numel(t)+(1-b).*pfun_monoexp(tau,t(end),t(tail_ind)-t(tail_ind(1)));
            monoexp_logdecays = log(pfun_monoexpBG(taus_pm,bgs_pm));
            
            [~,PM_ind] = max(tcspc(tail_ind)' * monoexp_logdecays',[],2);
            
            PM_tau = taus_pm(PM_ind);
            PM_bg = bgs_pm(PM_ind);
            
            irfparam(4) = PM_tau;
            irfparam(5) = PM_bg; 
        else
            irfparam(4) = 0;
        end
        %% IRF fit
        
        errmle = @(prop,counts)sum(-log(prop+1e-30).*counts);
        errfun = @(p) errmle(monoexpConvGamma(p,t(:)),tcspc(:));
        if check_tau.Value
            % fit alpha and beta/alpha instead of alpha and beta: This
            % parametrization performs better for far-off initial values.
            tempparam = irfparam(:)./[1; irfparam(1); 1; 1; 1];
            errfun = @(p) errmle(monoexpConvGamma(p(:).*[1; p(1); 1; 1; 1],t(:)),tcspc(:));
            tempparam = Simplex_Ntry(10,errfun,tempparam,[0 0.01 0 tempparam(4)*0.8 0],[1e2 1e2 inf tempparam(4)*1.2 1],1e-5,-800)';
            irfparam = tempparam(:).*[1; tempparam(1); 1; 1; 1];
%             irfparam = Simplex_Ntry(10,errfun,irfparam,[0 0 0 irfparam(4)*0.8 0],[1e3 1e3 inf irfparam(4)*1.2 1],1e-5,-800)';
            callback_FloatEdit(edit_tau,  [],irfparam(4),irfparam(4),[],'%g');
        else
            irfparam = Simplex_Ntry(10,errfun,irfparam,[0 0 0 nan 0],[1e3 1e3 inf nan 1],1e-5,-800)';            
        end
        
        callback_FloatEdit(edit_alpha,[],irfparam(1),irfparam(1),[],'%g');
        callback_FloatEdit(edit_beta, [],irfparam(2),irfparam(2),[],'%g');
        callback_FloatEdit(edit_t0,   [],irfparam(3),irfparam(3),[],'%g');
        callback_FloatEdit(edit_b,    [],irfparam(5),irfparam(5),[],'%g');
        
        %%
        plotIRF();
    end
end

function callback_export(~,~)
    acceptChanges = true;
    close(fig);
end
end

% Callback for edit fields containing floats. Checks if a correct
% number was entered and restricts it to the given bounds.
function callback_FloatEdit(hObj,~, minVal, maxVal, flag, formatstring)
    if nargin<3 || isempty(minVal)
        minVal=-inf;
    end
    if nargin<4 || isempty(maxVal)
        maxVal=inf;
    end
    if nargin<6 || isempty(formatstring)
        formatstring='%.2f';
    end

    value = str2num(get(hObj, 'String')); %#ok<ST2NM> str2num distinguishes NaN and invalid input, str2double does not.
    if isempty(value)
        set(hObj,'ForegroundColor','r');
        set(hObj,'String','INVALID');
        uicontrol(hObj);
    elseif nargin>4 && isnan(value) && ischar(flag) && strcmpi(flag,'includeNaN')
        set(hObj,'ForegroundColor','k');
        set(hObj,'String','NaN');
    else
        value = max(minVal,value);
        value = min(maxVal,value);
        set(hObj,'ForegroundColor','k');
        set(hObj,'String',sprintf(formatstring,value));
    end
end
%% Helper function
function y = gammaPDF(p,x)
    % shape, rate definition
    % Note with bg (==p(4)) the distribution is no longer normalised
    alpha = p(1);
    beta = p(2);
    x0 = 0;
    bg = 0;
    if numel(p)>=3
        x0 = p(3);
        if numel(p)>=4
            bg = p(4);
        end
    end
    x = x - x0;
    ind = x > 0;
    y = zeros(size(x)) + bg;
    y(ind) = (beta^alpha / gamma(alpha)) * x(ind).^(alpha-1) .* exp(-beta*x(ind)) + bg;
end

function y = monoexpConvGamma(p,t)
    % shape, rate definition, numerical normalised
    alpha = p(1);
    beta = p(2);
    t0 = p(3);
    tau = max(p(4),eps);
    b = p(5);
    
    y = exp(-t./tau);
    if sum(y)==0
        % Delta peak, eg for tau == 0
        y(1)=1; 
    end
    T = mean(diff(t))*numel(t);
    t = rem(t+T-t0,T); % set zero point to t0, assume repetivie boundry conditions
    
    irf = gammaPDF([alpha, beta],t);
    y = convolIRF(irf,y);
    y = b/numel(t) + (1-b)*y/sum(y);
end

function y = convolIRF(irf,y)
    y = real(ifft(fft(irf).*fft(y)));
end

function varargout = Simplex_Ntry(n,varargin)
    f_best = inf;
    for nidx = 1:n
        [x, dx, steps, f] = Simplex_Handle(varargin{:});
        if f<f_best
            varargout = {x, dx, steps, f};
            varargin{2} = (x(:)+varargin{2}(:))/2;
        end
    end
end
