function W_ISM_img = ISM_frw(data, psf)    
    %iamge size 
    [nx,ny] = size(data,[1 2]);
    %offset to zero
    psf = psf - min(psf,[],'all');
    %remove possible NaNs
    psf(isnan(psf)) = 0.0;
    %Fouier transform
    Fpsf = abs(fftshift(fft2(psf,nx,ny)));
    %normalise
    Fpsf = Fpsf./max(Fpsf,[],'all');

    %regulation parameter to prevent divergence
    eps = 0.04;
    %Fourier transform ISM image
    Fimg = fftshift(fft2(data));
    fa = abs(Fimg); % modulus
    fp = angle(Fimg); % Phase angle
    %calculate reweighting factor
    Ffull = 1./(Fpsf + eps);
    Ffull(Fpsf < eps) = 0;
    %only reweight Fourier amplitude not phase
    W_ISM_img = abs(ifft2(ifftshift(Ffull.*fa.*exp(1i.*fp))));
end