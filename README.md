# Data register 

All raw data files for the publication can be found in this repository.

Figure 2

    - bead images:    rawData\Figure 2\gattaQ_beads_atto647n_1p9mW_OD2_50nm-pix_4.ptu
    - shiftVectors:   rawData\Figure 2\STORM_test_gatta_quant_beadsR_50nm_27p9_pix_3.ptu

Figure 3

    - overview:       rawData\Figure 3\cos7_tubulin_Alexa647_50mM_MEA_power6p7_OD3p6_1.ptu
    - zoom in:        rawData\Figure 3\cos7_tubulin_Alexa647_50mM_MEA_power6p7_OD3p6_2.ptu
    - dSTORM:         rawData\Figure 3\cos7_tubulin_Alexa647_50mM_MEA_power6p7_OD1_1.ptu

Figure 4

    - overview:                         rawData\Figure 4\cos7_tub_alexa647_actin_jf646_50mM_MEA_50mM_MEA_1p85mW_OD0p6_roi4_2.ptu
    - zoom in:                          rawData\Figure 4\cos7_tub_alexa647_actin_jf646_50mM_MEA_50mM_MEA_1p85mW_OD0p6_roi4_3.ptu
    - dSTORM:                           rawData\Figure 4\cos7_tub_alexa647_actin_jf646_50mM_MEA_50mM_MEA_1p85mW_OD0p6_roi4_13.ptu
    - Alexa 647 reference pattern:      rawData\Figure 4\cos7_tubulin_Alexa647_50mM_MEA_power6p7_OD1_1.ptu
    - Janelia 646 reference pattern:    rawData\Figure 4\cos7_actin_Phal-JF646_50mM_MEA_glox_1p8mW_OD0p6_roi1_2.ptu

Figure 5

    - PAINT-PAINT:                  rawData\Figure 5\neurons_hom1_bass_cy3b_770pM_atto565_500pM_wl_roi2_22.ptu
    - Cy3b refernce pattern:        rawData\Figure 5\neurons_hom1_bass_cy3b_only_770pM_wl_roi1_8.ptu
    - Atto 565 reference pattern:   rawData\Figure 5\neurons_hom1_bass_atto565_only_770pM_wl_roi2_3.ptu
    

SI Figure 1:

    - dSTORM:   rawData\Figure 3\cos7_tubulin_Alexa647_50mM_MEA_power6p7_OD1_1.ptu

SI Figure 2:

    - panel d:  rawData\SI\cos7_tub_actin_alexa647_jf646_50mM_MEA_pbs_1p9mW_OD0p6_roi3_4.ptu

SI Figure 3:

    - overview: rawData\SI\cos7_tub_actin_alexa647_jf646_50mM_MEA_pbs_1p9mW_OD0p6_roi3_4.ptu
    - dSTORM:   rawData\SI\cos7_tub_actin_alexa647_jf646_50mM_MEA_pbs_1p9mW_OD0p6_roi3_15.ptu

SI Figure 4:

    - PAINT-PAINT:    rawData\Figure 5\neurons_hom1_bass_cy3b_770pM_atto565_500pM_wl_roi2_22.ptu


# callibration and TrackNTraceLT_ISM instructions

